--
-- File generated with SQLiteStudio v3.0.3 on Sun Apr 12 13:14:50 2015
--
-- Text encoding used: UTF-8
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: allergy
DROP TABLE IF EXISTS allergy;
CREATE TABLE allergy (allergy_id INTEGER PRIMARY KEY ASC AUTOINCREMENT, allergy_name VARCHAR NOT NULL, allergy_type INTEGER NOT NULL REFERENCES allergy_type (type_id), created_at DATETIME DEFAULT (CURRENT_TIMESTAMP), updated_at DATETIME DEFAULT (CURRENT_TIMESTAMP), lock_revision INTEGER)

-- Table: dispenser
DROP TABLE IF EXISTS dispenser;
CREATE TABLE dispenser (dispense_id INTEGER PRIMARY KEY ASC AUTOINCREMENT, dispense_date DATETIME DEFAULT (CURRENT_TIMESTAMP) NOT NULL, drug_dispensed INTEGER REFERENCES drug (drug_id) NOT NULL, patient INTEGER REFERENCES patient (patient_id) NOT NULL, dosage_given TEXT NOT NULL, created_at DATETIME DEFAULT (CURRENT_TIMESTAMP), updated_at DATETIME DEFAULT (CURRENT_TIMESTAMP), lock_revision INTEGER)

-- Table: allergy_type
DROP TABLE IF EXISTS allergy_type;
CREATE TABLE allergy_type (type_id INTEGER PRIMARY KEY ASC AUTOINCREMENT, type_name VARCHAR NOT NULL, created_at DATETIME DEFAULT (CURRENT_TIMESTAMP), updated_at DATETIME DEFAULT (CURRENT_TIMESTAMP), lock_revision INTEGER)

-- Table: role
DROP TABLE IF EXISTS role;
CREATE TABLE role (role_id INTEGER PRIMARY KEY ASC AUTOINCREMENT, role_name VARCHAR (100) NOT NULL, created_at DATETIME DEFAULT (CURRENT_TIMESTAMP), updated_at DATETIME DEFAULT (CURRENT_TIMESTAMP), lock_revision INTEGER)

-- Table: patient
DROP TABLE IF EXISTS patient;
CREATE TABLE patient (patient_id INTEGER PRIMARY KEY ASC AUTOINCREMENT, patient_lastname VARCHAR (100) NOT NULL, patient_othernames VARCHAR (255) NOT NULL, patient_dob DATE NOT NULL, patient_weight DOUBLE (5) NOT NULL, allergy INTEGER REFERENCES "sqlitestudio_temp_table" (allergy_id), created_at DATETIME DEFAULT (CURRENT_TIMESTAMP), updated_at DATETIME DEFAULT (CURRENT_TIMESTAMP), lock_revision INTEGER)

-- Table: drug
DROP TABLE IF EXISTS drug;
CREATE TABLE drug (drug_id INTEGER PRIMARY KEY ASC AUTOINCREMENT, brand_name VARCHAR NOT NULL, expiry_date DATE NOT NULL, ingredients TEXT NOT NULL, manufacturer VARCHAR NOT NULL, ailment TEXT NOT NULL, dosage TEXT NOT NULL, side_effects TEXT, created_at DATETIME DEFAULT (CURRENT_TIMESTAMP), updated_at DATETIME DEFAULT (CURRENT_TIMESTAMP), lock_revision INTEGER)

-- Table: user
DROP TABLE IF EXISTS user;
CREATE TABLE user (user_id INTEGER PRIMARY KEY ASC AUTOINCREMENT, user_lastname VARCHAR (100) NOT NULL, user_othernames VARCHAR (255) NOT NULL, username VARCHAR (40) UNIQUE NOT NULL, password VARCHAR (32) NOT NULL, role VARCHAR REFERENCES Role (role_id) ON DELETE CASCADE ON UPDATE CASCADE NOT NULL, created_at DATETIME DEFAULT (CURRENT_TIMESTAMP), updated_at DATETIME DEFAULT (CURRENT_TIMESTAMP), lock_revision INTEGER)

COMMIT TRANSACTION;

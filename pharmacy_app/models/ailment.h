#ifndef AILMENT_H
#define AILMENT_H

#include <QStringList>
#include <QDateTime>
#include <QVariant>
#include <QSharedDataPointer>
#include <TGlobal>
#include <TAbstractModel>

class TModelObject;
class AilmentObject;
class QJsonArray;


class T_MODEL_EXPORT Ailment : public TAbstractModel
{
public:
    Ailment();
    Ailment(const Ailment &other);
    Ailment(const AilmentObject &object);
    ~Ailment();

    int ailmentId() const;
    QString name() const;
    void setName(const QString &name);
    QString symptoms() const;
    void setSymptoms(const QString &symptoms);
    Ailment &operator=(const Ailment &other);

    bool create() { return TAbstractModel::create(); }
    bool update() { return TAbstractModel::update(); }
    bool save()   { return TAbstractModel::save(); }
    bool remove() { return TAbstractModel::remove(); }

    static Ailment create(const QString &name, const QString &symptoms);
    static Ailment create(const QVariantMap &values);
    static Ailment get(int ailmentId);
    static int count();
    static QList<Ailment> getAll();
    static QJsonArray getAllJson();

private:
    QSharedDataPointer<AilmentObject> d;

    TModelObject *modelData();
    const TModelObject *modelData() const;
};

Q_DECLARE_METATYPE(Ailment)
Q_DECLARE_METATYPE(QList<Ailment>)

#endif // AILMENT_H

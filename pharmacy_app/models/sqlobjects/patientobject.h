#ifndef PATIENTOBJECT_H
#define PATIENTOBJECT_H

#include <TSqlObject>
#include <QSharedData>


class T_MODEL_EXPORT PatientObject : public TSqlObject, public QSharedData
{
public:
    int patient_id;
    QString patient_lastname;
    QString patient_othernames;
    QString patient_dob;
    QString patient_gender;
    QString patient_weight;
    int allergy;
    QString created_at;
    QString updated_at;
    int lock_revision;

    enum PropertyIndex {
        PatientId = 0,
        PatientLastname,
        PatientOthernames,
        PatientDob,
        PatientGender,
        PatientWeight,
        Allergy,
        CreatedAt,
        UpdatedAt,
        LockRevision,
    };

    int primaryKeyIndex() const { return PatientId; }
    int autoValueIndex() const { return PatientId; }
    QString tableName() const { return QLatin1String("patient"); }

private:    /*** Don't modify below this line ***/
    Q_OBJECT
    Q_PROPERTY(int patient_id READ getpatient_id WRITE setpatient_id)
    T_DEFINE_PROPERTY(int, patient_id)
    Q_PROPERTY(QString patient_lastname READ getpatient_lastname WRITE setpatient_lastname)
    T_DEFINE_PROPERTY(QString, patient_lastname)
    Q_PROPERTY(QString patient_othernames READ getpatient_othernames WRITE setpatient_othernames)
    T_DEFINE_PROPERTY(QString, patient_othernames)
    Q_PROPERTY(QString patient_dob READ getpatient_dob WRITE setpatient_dob)
    T_DEFINE_PROPERTY(QString, patient_dob)
    Q_PROPERTY(QString patient_gender READ getpatient_gender WRITE setpatient_gender)
    T_DEFINE_PROPERTY(QString, patient_gender)
    Q_PROPERTY(QString patient_weight READ getpatient_weight WRITE setpatient_weight)
    T_DEFINE_PROPERTY(QString, patient_weight)
    Q_PROPERTY(int allergy READ getallergy WRITE setallergy)
    T_DEFINE_PROPERTY(int, allergy)
    Q_PROPERTY(QString created_at READ getcreated_at WRITE setcreated_at)
    T_DEFINE_PROPERTY(QString, created_at)
    Q_PROPERTY(QString updated_at READ getupdated_at WRITE setupdated_at)
    T_DEFINE_PROPERTY(QString, updated_at)
    Q_PROPERTY(int lock_revision READ getlock_revision WRITE setlock_revision)
    T_DEFINE_PROPERTY(int, lock_revision)
};

#endif // PATIENTOBJECT_H

#ifndef DISPENSEROBJECT_H
#define DISPENSEROBJECT_H

#include <TSqlObject>
#include <QSharedData>


class T_MODEL_EXPORT DispenserObject : public TSqlObject, public QSharedData
{
public:
    int dispense_id;
    QString dispense_date;
    int patient;
    QString ailment;
    int drug_dispensed;
    QString dosage_given;
    QString created_at;
    QString updated_at;
    int lock_revision;

    enum PropertyIndex {
        DispenseId = 0,
        DispenseDate,
        Patient,
        Ailment,
        DrugDispensed,
        DosageGiven,
        CreatedAt,
        UpdatedAt,
        LockRevision,
    };

    int primaryKeyIndex() const { return DispenseId; }
    int autoValueIndex() const { return DispenseId; }
    QString tableName() const { return QLatin1String("dispenser"); }

private:    /*** Don't modify below this line ***/
    Q_OBJECT
    Q_PROPERTY(int dispense_id READ getdispense_id WRITE setdispense_id)
    T_DEFINE_PROPERTY(int, dispense_id)
    Q_PROPERTY(QString dispense_date READ getdispense_date WRITE setdispense_date)
    T_DEFINE_PROPERTY(QString, dispense_date)
    Q_PROPERTY(int patient READ getpatient WRITE setpatient)
    T_DEFINE_PROPERTY(int, patient)
    Q_PROPERTY(QString ailment READ getailment WRITE setailment)
    T_DEFINE_PROPERTY(QString, ailment)
    Q_PROPERTY(int drug_dispensed READ getdrug_dispensed WRITE setdrug_dispensed)
    T_DEFINE_PROPERTY(int, drug_dispensed)
    Q_PROPERTY(QString dosage_given READ getdosage_given WRITE setdosage_given)
    T_DEFINE_PROPERTY(QString, dosage_given)
    Q_PROPERTY(QString created_at READ getcreated_at WRITE setcreated_at)
    T_DEFINE_PROPERTY(QString, created_at)
    Q_PROPERTY(QString updated_at READ getupdated_at WRITE setupdated_at)
    T_DEFINE_PROPERTY(QString, updated_at)
    Q_PROPERTY(int lock_revision READ getlock_revision WRITE setlock_revision)
    T_DEFINE_PROPERTY(int, lock_revision)
};

#endif // DISPENSEROBJECT_H

#ifndef ALLERGYOBJECT_H
#define ALLERGYOBJECT_H

#include <TSqlObject>
#include <QSharedData>


class T_MODEL_EXPORT AllergyObject : public TSqlObject, public QSharedData
{
public:
    int allergy_id;
    QString allergy_name;
    int allergy_type;
    QString description;
    QString created_at;
    QString updated_at;
    int lock_revision;

    enum PropertyIndex {
        AllergyId = 0,
        AllergyName,
        AllergyType,
        Description,
        CreatedAt,
        UpdatedAt,
        LockRevision,
    };

    int primaryKeyIndex() const { return AllergyId; }
    int autoValueIndex() const { return AllergyId; }
    QString tableName() const { return QLatin1String("allergy"); }

private:    /*** Don't modify below this line ***/
    Q_OBJECT
    Q_PROPERTY(int allergy_id READ getallergy_id WRITE setallergy_id)
    T_DEFINE_PROPERTY(int, allergy_id)
    Q_PROPERTY(QString allergy_name READ getallergy_name WRITE setallergy_name)
    T_DEFINE_PROPERTY(QString, allergy_name)
    Q_PROPERTY(int allergy_type READ getallergy_type WRITE setallergy_type)
    T_DEFINE_PROPERTY(int, allergy_type)
    Q_PROPERTY(QString description READ getdescription WRITE setdescription)
    T_DEFINE_PROPERTY(QString, description)
    Q_PROPERTY(QString created_at READ getcreated_at WRITE setcreated_at)
    T_DEFINE_PROPERTY(QString, created_at)
    Q_PROPERTY(QString updated_at READ getupdated_at WRITE setupdated_at)
    T_DEFINE_PROPERTY(QString, updated_at)
    Q_PROPERTY(int lock_revision READ getlock_revision WRITE setlock_revision)
    T_DEFINE_PROPERTY(int, lock_revision)
};

#endif // ALLERGYOBJECT_H

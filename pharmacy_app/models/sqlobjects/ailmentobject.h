#ifndef AILMENTOBJECT_H
#define AILMENTOBJECT_H

#include <TSqlObject>
#include <QSharedData>


class T_MODEL_EXPORT AilmentObject : public TSqlObject, public QSharedData
{
public:
    int ailment_id;
    QString name;
    QString symptoms;

    enum PropertyIndex {
        AilmentId = 0,
        Name,
        Symptoms,
    };

    int primaryKeyIndex() const { return AilmentId; }
    int autoValueIndex() const { return AilmentId; }
    QString tableName() const { return QLatin1String("ailment"); }

private:    /*** Don't modify below this line ***/
    Q_OBJECT
    Q_PROPERTY(int ailment_id READ getailment_id WRITE setailment_id)
    T_DEFINE_PROPERTY(int, ailment_id)
    Q_PROPERTY(QString name READ getname WRITE setname)
    T_DEFINE_PROPERTY(QString, name)
    Q_PROPERTY(QString symptoms READ getsymptoms WRITE setsymptoms)
    T_DEFINE_PROPERTY(QString, symptoms)
};

#endif // AILMENTOBJECT_H

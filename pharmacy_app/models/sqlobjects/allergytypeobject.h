#ifndef ALLERGYTYPEOBJECT_H
#define ALLERGYTYPEOBJECT_H

#include <TSqlObject>
#include <QSharedData>


class T_MODEL_EXPORT AllergyTypeObject : public TSqlObject, public QSharedData
{
public:
    int type_id;
    QString type_name;
    QString description;
    QString created_at;
    QString updated_at;
    int lock_revision;

    enum PropertyIndex {
        TypeId = 0,
        TypeName,
        Description,
        CreatedAt,
        UpdatedAt,
        LockRevision,
    };

    int primaryKeyIndex() const { return TypeId; }
    int autoValueIndex() const { return TypeId; }
    QString tableName() const { return QLatin1String("allergy_type"); }

private:    /*** Don't modify below this line ***/
    Q_OBJECT
    Q_PROPERTY(int type_id READ gettype_id WRITE settype_id)
    T_DEFINE_PROPERTY(int, type_id)
    Q_PROPERTY(QString type_name READ gettype_name WRITE settype_name)
    T_DEFINE_PROPERTY(QString, type_name)
    Q_PROPERTY(QString description READ getdescription WRITE setdescription)
    T_DEFINE_PROPERTY(QString, description)
    Q_PROPERTY(QString created_at READ getcreated_at WRITE setcreated_at)
    T_DEFINE_PROPERTY(QString, created_at)
    Q_PROPERTY(QString updated_at READ getupdated_at WRITE setupdated_at)
    T_DEFINE_PROPERTY(QString, updated_at)
    Q_PROPERTY(int lock_revision READ getlock_revision WRITE setlock_revision)
    T_DEFINE_PROPERTY(int, lock_revision)
};

#endif // ALLERGYTYPEOBJECT_H

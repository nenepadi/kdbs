#ifndef ROLEOBJECT_H
#define ROLEOBJECT_H

#include <TSqlObject>
#include <QSharedData>


class T_MODEL_EXPORT RoleObject : public TSqlObject, public QSharedData
{
public:
    int role_id;
    QString role_name;
    QString created_at;
    QString updated_at;
    int lock_revision;

    enum PropertyIndex {
        RoleId = 0,
        RoleName,
        CreatedAt,
        UpdatedAt,
        LockRevision,
    };

    int primaryKeyIndex() const { return RoleId; }
    int autoValueIndex() const { return RoleId; }
    QString tableName() const { return QLatin1String("role"); }

private:    /*** Don't modify below this line ***/
    Q_OBJECT
    Q_PROPERTY(int role_id READ getrole_id WRITE setrole_id)
    T_DEFINE_PROPERTY(int, role_id)
    Q_PROPERTY(QString role_name READ getrole_name WRITE setrole_name)
    T_DEFINE_PROPERTY(QString, role_name)
    Q_PROPERTY(QString created_at READ getcreated_at WRITE setcreated_at)
    T_DEFINE_PROPERTY(QString, created_at)
    Q_PROPERTY(QString updated_at READ getupdated_at WRITE setupdated_at)
    T_DEFINE_PROPERTY(QString, updated_at)
    Q_PROPERTY(int lock_revision READ getlock_revision WRITE setlock_revision)
    T_DEFINE_PROPERTY(int, lock_revision)
};

#endif // ROLEOBJECT_H

#ifndef USEROBJECT_H
#define USEROBJECT_H

#include <TSqlObject>
#include <QSharedData>


class T_MODEL_EXPORT UserObject : public TSqlObject, public QSharedData
{
public:
    int user_id;
    QString user_lastname;
    QString user_othernames;
    QString username;
    QString password;
    int role;
    QString created_at;
    QString updated_at;
    int lock_revision;

    enum PropertyIndex {
        UserId = 0,
        UserLastname,
        UserOthernames,
        Username,
        Password,
        Role,
        CreatedAt,
        UpdatedAt,
        LockRevision,
    };

    int primaryKeyIndex() const { return UserId; }
    int autoValueIndex() const { return UserId; }
    QString tableName() const { return QLatin1String("user"); }

private:    /*** Don't modify below this line ***/
    Q_OBJECT
    Q_PROPERTY(int user_id READ getuser_id WRITE setuser_id)
    T_DEFINE_PROPERTY(int, user_id)
    Q_PROPERTY(QString user_lastname READ getuser_lastname WRITE setuser_lastname)
    T_DEFINE_PROPERTY(QString, user_lastname)
    Q_PROPERTY(QString user_othernames READ getuser_othernames WRITE setuser_othernames)
    T_DEFINE_PROPERTY(QString, user_othernames)
    Q_PROPERTY(QString username READ getusername WRITE setusername)
    T_DEFINE_PROPERTY(QString, username)
    Q_PROPERTY(QString password READ getpassword WRITE setpassword)
    T_DEFINE_PROPERTY(QString, password)
    Q_PROPERTY(int role READ getrole WRITE setrole)
    T_DEFINE_PROPERTY(int, role)
    Q_PROPERTY(QString created_at READ getcreated_at WRITE setcreated_at)
    T_DEFINE_PROPERTY(QString, created_at)
    Q_PROPERTY(QString updated_at READ getupdated_at WRITE setupdated_at)
    T_DEFINE_PROPERTY(QString, updated_at)
    Q_PROPERTY(int lock_revision READ getlock_revision WRITE setlock_revision)
    T_DEFINE_PROPERTY(int, lock_revision)
};

#endif // USEROBJECT_H

#ifndef DRUGOBJECT_H
#define DRUGOBJECT_H

#include <TSqlObject>
#include <QSharedData>


class T_MODEL_EXPORT DrugObject : public TSqlObject, public QSharedData
{
public:
    int drug_id;
    QString brand_name;
    QString expiry_date;
    QString ingredients;
    QString manufacturer;
    QString ailment;
    QString dosage;
    QString dosage_children;
    QString dosage_adult;
    QString side_effects;
    QString created_at;
    QString updated_at;
    int lock_revision;

    enum PropertyIndex {
        DrugId = 0,
        BrandName,
        ExpiryDate,
        Ingredients,
        Manufacturer,
        Ailment,
        Dosage,
        DosageChildren,
        DosageAdult,
        SideEffects,
        CreatedAt,
        UpdatedAt,
        LockRevision,
    };

    int primaryKeyIndex() const { return DrugId; }
    int autoValueIndex() const { return DrugId; }
    QString tableName() const { return QLatin1String("drug"); }

private:    /*** Don't modify below this line ***/
    Q_OBJECT
    Q_PROPERTY(int drug_id READ getdrug_id WRITE setdrug_id)
    T_DEFINE_PROPERTY(int, drug_id)
    Q_PROPERTY(QString brand_name READ getbrand_name WRITE setbrand_name)
    T_DEFINE_PROPERTY(QString, brand_name)
    Q_PROPERTY(QString expiry_date READ getexpiry_date WRITE setexpiry_date)
    T_DEFINE_PROPERTY(QString, expiry_date)
    Q_PROPERTY(QString ingredients READ getingredients WRITE setingredients)
    T_DEFINE_PROPERTY(QString, ingredients)
    Q_PROPERTY(QString manufacturer READ getmanufacturer WRITE setmanufacturer)
    T_DEFINE_PROPERTY(QString, manufacturer)
    Q_PROPERTY(QString ailment READ getailment WRITE setailment)
    T_DEFINE_PROPERTY(QString, ailment)
    Q_PROPERTY(QString dosage READ getdosage WRITE setdosage)
    T_DEFINE_PROPERTY(QString, dosage)
    Q_PROPERTY(QString dosage_children READ getdosage_children WRITE setdosage_children)
    T_DEFINE_PROPERTY(QString, dosage_children)
    Q_PROPERTY(QString dosage_adult READ getdosage_adult WRITE setdosage_adult)
    T_DEFINE_PROPERTY(QString, dosage_adult)
    Q_PROPERTY(QString side_effects READ getside_effects WRITE setside_effects)
    T_DEFINE_PROPERTY(QString, side_effects)
    Q_PROPERTY(QString created_at READ getcreated_at WRITE setcreated_at)
    T_DEFINE_PROPERTY(QString, created_at)
    Q_PROPERTY(QString updated_at READ getupdated_at WRITE setupdated_at)
    T_DEFINE_PROPERTY(QString, updated_at)
    Q_PROPERTY(int lock_revision READ getlock_revision WRITE setlock_revision)
    T_DEFINE_PROPERTY(int, lock_revision)
};

#endif // DRUGOBJECT_H

#ifndef DISPENSER_H
#define DISPENSER_H

#include <QStringList>
#include <QDateTime>
#include <QVariant>
#include <QSharedDataPointer>
#include <TGlobal>
#include <TAbstractModel>

class TModelObject;
class DispenserObject;
class QJsonArray;


class T_MODEL_EXPORT Dispenser : public TAbstractModel
{
public:
    Dispenser();
    Dispenser(const Dispenser &other);
    Dispenser(const DispenserObject &object);
    ~Dispenser();

    int dispenseId() const;
    QString dispenseDate() const;
    void setDispenseDate(const QString &dispenseDate);
    int patient() const;
    void setPatient(int patient);
    QString ailment() const;
    void setAilment(const QString &ailment);
    int drugDispensed() const;
    void setDrugDispensed(int drugDispensed);
    QString dosageGiven() const;
    void setDosageGiven(const QString &dosageGiven);
    QString createdAt() const;
    QString updatedAt() const;
    int lockRevision() const;
    Dispenser &operator=(const Dispenser &other);

    bool create() { return TAbstractModel::create(); }
    bool update() { return TAbstractModel::update(); }
    bool save()   { return TAbstractModel::save(); }
    bool remove() { return TAbstractModel::remove(); }

    static Dispenser create(const QString &dispenseDate, int patient, const QString &ailment, int drugDispensed, const QString &dosageGiven);
    static Dispenser create(const QVariantMap &values);
    static Dispenser get(int dispenseId);
    static Dispenser get(int dispenseId, int lockRevision);
    static int count();
    static QList<Dispenser> getAll();
    static QJsonArray getAllJson();

private:
    QSharedDataPointer<DispenserObject> d;

    TModelObject *modelData();
    const TModelObject *modelData() const;
};

Q_DECLARE_METATYPE(Dispenser)
Q_DECLARE_METATYPE(QList<Dispenser>)

#endif // DISPENSER_H

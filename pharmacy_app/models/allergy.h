#ifndef ALLERGY_H
#define ALLERGY_H

#include <QStringList>
#include <QDateTime>
#include <QVariant>
#include <QSharedDataPointer>
#include <TGlobal>
#include <TAbstractModel>

class TModelObject;
class AllergyObject;
class QJsonArray;


class T_MODEL_EXPORT Allergy : public TAbstractModel
{
public:
    Allergy();
    Allergy(const Allergy &other);
    Allergy(const AllergyObject &object);
    ~Allergy();

    int allergyId() const;
    QString allergyName() const;
    void setAllergyName(const QString &allergyName);
    int allergyType() const;
    void setAllergyType(int allergyType);
    QString description() const;
    void setDescription(const QString &description);
    QString createdAt() const;
    QString updatedAt() const;
    int lockRevision() const;
    Allergy &operator=(const Allergy &other);

    bool create() { return TAbstractModel::create(); }
    bool update() { return TAbstractModel::update(); }
    bool save()   { return TAbstractModel::save(); }
    bool remove() { return TAbstractModel::remove(); }

    static Allergy create(const QString &allergyName, int allergyType, const QString &description);
    static Allergy create(const QVariantMap &values);
    static Allergy get(int allergyId);
    static Allergy get(int allergyId, int lockRevision);
    static int count();
    static QList<Allergy> getAll();
    static QJsonArray getAllJson();

private:
    QSharedDataPointer<AllergyObject> d;

    TModelObject *modelData();
    const TModelObject *modelData() const;
};

Q_DECLARE_METATYPE(Allergy)
Q_DECLARE_METATYPE(QList<Allergy>)

#endif // ALLERGY_H

#ifndef ROLE_H
#define ROLE_H

#include <QStringList>
#include <QDateTime>
#include <QVariant>
#include <QSharedDataPointer>
#include <TGlobal>
#include <TAbstractModel>

class TModelObject;
class RoleObject;
class QJsonArray;


class T_MODEL_EXPORT Role : public TAbstractModel
{
public:
    Role();
    Role(const Role &other);
    Role(const RoleObject &object);
    ~Role();

    int roleId() const;
    QString roleName() const;
    void setRoleName(const QString &roleName);
    QString createdAt() const;
    QString updatedAt() const;
    int lockRevision() const;
    Role &operator=(const Role &other);

    bool create() { return TAbstractModel::create(); }
    bool update() { return TAbstractModel::update(); }
    bool save()   { return TAbstractModel::save(); }
    bool remove() { return TAbstractModel::remove(); }

    static Role create(const QString &roleName);
    static Role create(const QVariantMap &values);
    static Role get(int roleId);
    static Role get(int roleId, int lockRevision);
    static int count();
    static QList<Role> getAll();
    static QJsonArray getAllJson();

private:
    QSharedDataPointer<RoleObject> d;

    TModelObject *modelData();
    const TModelObject *modelData() const;
};

Q_DECLARE_METATYPE(Role)
Q_DECLARE_METATYPE(QList<Role>)

#endif // ROLE_H

#ifndef DRUG_H
#define DRUG_H

#include <QStringList>
#include <QDateTime>
#include <QVariant>
#include <QSharedDataPointer>
#include <TGlobal>
#include <TAbstractModel>

class TModelObject;
class DrugObject;
class QJsonArray;


class T_MODEL_EXPORT Drug : public TAbstractModel
{
public:
    Drug();
    Drug(const Drug &other);
    Drug(const DrugObject &object);
    ~Drug();

    int drugId() const;
    QString brandName() const;
    void setBrandName(const QString &brandName);
    QString expiryDate() const;
    void setExpiryDate(const QString &expiryDate);
    QString ingredients() const;
    void setIngredients(const QString &ingredients);
    QString manufacturer() const;
    void setManufacturer(const QString &manufacturer);
    QString ailment() const;
    void setAilment(const QString &ailment);
    QString dosage() const;
    void setDosage(const QString &dosage);
    QString dosageChildren() const;
    void setDosageChildren(const QString &dosageChildren);
    QString dosageAdult() const;
    void setDosageAdult(const QString &dosageAdult);
    QString sideEffects() const;
    void setSideEffects(const QString &sideEffects);
    QString createdAt() const;
    QString updatedAt() const;
    int lockRevision() const;
    Drug &operator=(const Drug &other);

    bool create() { return TAbstractModel::create(); }
    bool update() { return TAbstractModel::update(); }
    bool save()   { return TAbstractModel::save(); }
    bool remove() { return TAbstractModel::remove(); }

    static Drug create(const QString &brandName, const QString &expiryDate, const QString &ingredients, const QString &manufacturer, const QString &ailment, const QString &dosage, const QString &dosageChildren, const QString &dosageAdult, const QString &sideEffects);
    static Drug create(const QVariantMap &values);
    static Drug get(int drugId);
    static Drug get(int drugId, int lockRevision);
    static int count();
    static QList<Drug> getAll();
    static QJsonArray getAllJson();

private:
    QSharedDataPointer<DrugObject> d;

    TModelObject *modelData();
    const TModelObject *modelData() const;
};

Q_DECLARE_METATYPE(Drug)
Q_DECLARE_METATYPE(QList<Drug>)

#endif // DRUG_H

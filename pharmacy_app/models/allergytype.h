#ifndef ALLERGYTYPE_H
#define ALLERGYTYPE_H

#include <QStringList>
#include <QDateTime>
#include <QVariant>
#include <QSharedDataPointer>
#include <TGlobal>
#include <TAbstractModel>

class TModelObject;
class AllergyTypeObject;
class QJsonArray;


class T_MODEL_EXPORT AllergyType : public TAbstractModel
{
public:
    AllergyType();
    AllergyType(const AllergyType &other);
    AllergyType(const AllergyTypeObject &object);
    ~AllergyType();

    int typeId() const;
    QString typeName() const;
    void setTypeName(const QString &typeName);
    QString description() const;
    void setDescription(const QString &description);
    QString createdAt() const;
    QString updatedAt() const;
    int lockRevision() const;
    AllergyType &operator=(const AllergyType &other);

    bool create() { return TAbstractModel::create(); }
    bool update() { return TAbstractModel::update(); }
    bool save()   { return TAbstractModel::save(); }
    bool remove() { return TAbstractModel::remove(); }

    static AllergyType create(const QString &typeName, const QString &description);
    static AllergyType create(const QVariantMap &values);
    static AllergyType get(int typeId);
    static AllergyType get(int typeId, int lockRevision);
    static int count();
    static QList<AllergyType> getAll();
    static QJsonArray getAllJson();

private:
    QSharedDataPointer<AllergyTypeObject> d;

    TModelObject *modelData();
    const TModelObject *modelData() const;
};

Q_DECLARE_METATYPE(AllergyType)
Q_DECLARE_METATYPE(QList<AllergyType>)

#endif // ALLERGYTYPE_H

#include <TreeFrogModel>
#include "role.h"
#include "roleobject.h"

Role::Role()
    : TAbstractModel(), d(new RoleObject)
{
    d->role_id = 0;
    d->lock_revision = 0;
}

Role::Role(const Role &other)
    : TAbstractModel(), d(new RoleObject(*other.d))
{ }

Role::Role(const RoleObject &object)
    : TAbstractModel(), d(new RoleObject(object))
{ }

Role::~Role()
{
    // If the reference count becomes 0,
    // the shared data object 'RoleObject' is deleted.
}

int Role::roleId() const
{
    return d->role_id;
}

QString Role::roleName() const
{
    return d->role_name;
}

void Role::setRoleName(const QString &roleName)
{
    d->role_name = roleName;
}

QString Role::createdAt() const
{
    return d->created_at;
}

QString Role::updatedAt() const
{
    return d->updated_at;
}

int Role::lockRevision() const
{
    return d->lock_revision;
}

Role &Role::operator=(const Role &other)
{
    d = other.d;  // increments the reference count of the data
    return *this;
}

Role Role::create(const QString &roleName)
{
    RoleObject obj;
    obj.role_name = roleName;
    if (!obj.create()) {
        return Role();
    }
    return Role(obj);
}

Role Role::create(const QVariantMap &values)
{
    Role model;
    model.setProperties(values);
    if (!model.d->create()) {
        model.d->clear();
    }
    return model;
}

Role Role::get(int roleId)
{
    TSqlORMapper<RoleObject> mapper;
    return Role(mapper.findByPrimaryKey(roleId));
}

Role Role::get(int roleId, int lockRevision)
{
    TSqlORMapper<RoleObject> mapper;
    TCriteria cri;
    cri.add(RoleObject::RoleId, roleId);
    cri.add(RoleObject::LockRevision, lockRevision);
    return Role(mapper.findFirst(cri));
}

int Role::count()
{
    TSqlORMapper<RoleObject> mapper;
    return mapper.findCount();
}

QList<Role> Role::getAll()
{
    return tfGetModelListByCriteria<Role, RoleObject>(TCriteria());
}

QJsonArray Role::getAllJson()
{
    QJsonArray array;
    TSqlORMapper<RoleObject> mapper;

    if (mapper.find() > 0) {
        for (TSqlORMapperIterator<RoleObject> i(mapper); i.hasNext(); ) {
            array.append(QJsonValue(QJsonObject::fromVariantMap(Role(i.next()).toVariantMap())));
        }
    }
    return array;
}

TModelObject *Role::modelData()
{
    return d.data();
}

const TModelObject *Role::modelData() const
{
    return d.data();
}

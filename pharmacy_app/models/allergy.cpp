#include <TreeFrogModel>
#include "allergy.h"
#include "allergyobject.h"

Allergy::Allergy()
    : TAbstractModel(), d(new AllergyObject)
{
    d->allergy_id = 0;
    d->allergy_type = 0;
    d->lock_revision = 0;
}

Allergy::Allergy(const Allergy &other)
    : TAbstractModel(), d(new AllergyObject(*other.d))
{ }

Allergy::Allergy(const AllergyObject &object)
    : TAbstractModel(), d(new AllergyObject(object))
{ }

Allergy::~Allergy()
{
    // If the reference count becomes 0,
    // the shared data object 'AllergyObject' is deleted.
}

int Allergy::allergyId() const
{
    return d->allergy_id;
}

QString Allergy::allergyName() const
{
    return d->allergy_name;
}

void Allergy::setAllergyName(const QString &allergyName)
{
    d->allergy_name = allergyName;
}

int Allergy::allergyType() const
{
    return d->allergy_type;
}

void Allergy::setAllergyType(int allergyType)
{
    d->allergy_type = allergyType;
}

QString Allergy::description() const
{
    return d->description;
}

void Allergy::setDescription(const QString &description)
{
    d->description = description;
}

QString Allergy::createdAt() const
{
    return d->created_at;
}

QString Allergy::updatedAt() const
{
    return d->updated_at;
}

int Allergy::lockRevision() const
{
    return d->lock_revision;
}

Allergy &Allergy::operator=(const Allergy &other)
{
    d = other.d;  // increments the reference count of the data
    return *this;
}

Allergy Allergy::create(const QString &allergyName, int allergyType, const QString &description)
{
    AllergyObject obj;
    obj.allergy_name = allergyName;
    obj.allergy_type = allergyType;
    obj.description = description;
    if (!obj.create()) {
        return Allergy();
    }
    return Allergy(obj);
}

Allergy Allergy::create(const QVariantMap &values)
{
    Allergy model;
    model.setProperties(values);
    if (!model.d->create()) {
        model.d->clear();
    }
    return model;
}

Allergy Allergy::get(int allergyId)
{
    TSqlORMapper<AllergyObject> mapper;
    return Allergy(mapper.findByPrimaryKey(allergyId));
}

Allergy Allergy::get(int allergyId, int lockRevision)
{
    TSqlORMapper<AllergyObject> mapper;
    TCriteria cri;
    cri.add(AllergyObject::AllergyId, allergyId);
    cri.add(AllergyObject::LockRevision, lockRevision);
    return Allergy(mapper.findFirst(cri));
}

int Allergy::count()
{
    TSqlORMapper<AllergyObject> mapper;
    return mapper.findCount();
}

QList<Allergy> Allergy::getAll()
{
    return tfGetModelListByCriteria<Allergy, AllergyObject>(TCriteria());
}

QJsonArray Allergy::getAllJson()
{
    QJsonArray array;
    TSqlORMapper<AllergyObject> mapper;

    if (mapper.find() > 0) {
        for (TSqlORMapperIterator<AllergyObject> i(mapper); i.hasNext(); ) {
            array.append(QJsonValue(QJsonObject::fromVariantMap(Allergy(i.next()).toVariantMap())));
        }
    }
    return array;
}

TModelObject *Allergy::modelData()
{
    return d.data();
}

const TModelObject *Allergy::modelData() const
{
    return d.data();
}

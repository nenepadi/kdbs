#include <TreeFrogModel>
#include "drug.h"
#include "drugobject.h"

Drug::Drug()
    : TAbstractModel(), d(new DrugObject)
{
    d->drug_id = 0;
    d->lock_revision = 0;
}

Drug::Drug(const Drug &other)
    : TAbstractModel(), d(new DrugObject(*other.d))
{ }

Drug::Drug(const DrugObject &object)
    : TAbstractModel(), d(new DrugObject(object))
{ }

Drug::~Drug()
{
    // If the reference count becomes 0,
    // the shared data object 'DrugObject' is deleted.
}

int Drug::drugId() const
{
    return d->drug_id;
}

QString Drug::brandName() const
{
    return d->brand_name;
}

void Drug::setBrandName(const QString &brandName)
{
    d->brand_name = brandName;
}

QString Drug::expiryDate() const
{
    return d->expiry_date;
}

void Drug::setExpiryDate(const QString &expiryDate)
{
    d->expiry_date = expiryDate;
}

QString Drug::ingredients() const
{
    return d->ingredients;
}

void Drug::setIngredients(const QString &ingredients)
{
    d->ingredients = ingredients;
}

QString Drug::manufacturer() const
{
    return d->manufacturer;
}

void Drug::setManufacturer(const QString &manufacturer)
{
    d->manufacturer = manufacturer;
}

QString Drug::ailment() const
{
    return d->ailment;
}

void Drug::setAilment(const QString &ailment)
{
    d->ailment = ailment;
}

QString Drug::dosage() const
{
    return d->dosage;
}

void Drug::setDosage(const QString &dosage)
{
    d->dosage = dosage;
}

QString Drug::dosageChildren() const
{
    return d->dosage_children;
}

void Drug::setDosageChildren(const QString &dosageChildren)
{
    d->dosage_children = dosageChildren;
}

QString Drug::dosageAdult() const
{
    return d->dosage_adult;
}

void Drug::setDosageAdult(const QString &dosageAdult)
{
    d->dosage_adult = dosageAdult;
}

QString Drug::sideEffects() const
{
    return d->side_effects;
}

void Drug::setSideEffects(const QString &sideEffects)
{
    d->side_effects = sideEffects;
}

QString Drug::createdAt() const
{
    return d->created_at;
}

QString Drug::updatedAt() const
{
    return d->updated_at;
}

int Drug::lockRevision() const
{
    return d->lock_revision;
}

Drug &Drug::operator=(const Drug &other)
{
    d = other.d;  // increments the reference count of the data
    return *this;
}

Drug Drug::create(const QString &brandName, const QString &expiryDate, const QString &ingredients, const QString &manufacturer, const QString &ailment, const QString &dosage, const QString &dosageChildren, const QString &dosageAdult, const QString &sideEffects)
{
    DrugObject obj;
    obj.brand_name = brandName;
    obj.expiry_date = expiryDate;
    obj.ingredients = ingredients;
    obj.manufacturer = manufacturer;
    obj.ailment = ailment;
    obj.dosage = dosage;
    obj.dosage_children = dosageChildren;
    obj.dosage_adult = dosageAdult;
    obj.side_effects = sideEffects;
    if (!obj.create()) {
        return Drug();
    }
    return Drug(obj);
}

Drug Drug::create(const QVariantMap &values)
{
    Drug model;
    model.setProperties(values);
    if (!model.d->create()) {
        model.d->clear();
    }
    return model;
}

Drug Drug::get(int drugId)
{
    TSqlORMapper<DrugObject> mapper;
    return Drug(mapper.findByPrimaryKey(drugId));
}

Drug Drug::get(int drugId, int lockRevision)
{
    TSqlORMapper<DrugObject> mapper;
    TCriteria cri;
    cri.add(DrugObject::DrugId, drugId);
    cri.add(DrugObject::LockRevision, lockRevision);
    return Drug(mapper.findFirst(cri));
}

int Drug::count()
{
    TSqlORMapper<DrugObject> mapper;
    return mapper.findCount();
}

QList<Drug> Drug::getAll()
{
    return tfGetModelListByCriteria<Drug, DrugObject>(TCriteria());
}

QJsonArray Drug::getAllJson()
{
    QJsonArray array;
    TSqlORMapper<DrugObject> mapper;

    if (mapper.find() > 0) {
        for (TSqlORMapperIterator<DrugObject> i(mapper); i.hasNext(); ) {
            array.append(QJsonValue(QJsonObject::fromVariantMap(Drug(i.next()).toVariantMap())));
        }
    }
    return array;
}

TModelObject *Drug::modelData()
{
    return d.data();
}

const TModelObject *Drug::modelData() const
{
    return d.data();
}

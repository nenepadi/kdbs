#include <TreeFrogModel>
#include "dispenser.h"
#include "dispenserobject.h"

Dispenser::Dispenser()
    : TAbstractModel(), d(new DispenserObject)
{
    d->dispense_id = 0;
    d->patient = 0;
    d->drug_dispensed = 0;
    d->lock_revision = 0;
}

Dispenser::Dispenser(const Dispenser &other)
    : TAbstractModel(), d(new DispenserObject(*other.d))
{ }

Dispenser::Dispenser(const DispenserObject &object)
    : TAbstractModel(), d(new DispenserObject(object))
{ }

Dispenser::~Dispenser()
{
    // If the reference count becomes 0,
    // the shared data object 'DispenserObject' is deleted.
}

int Dispenser::dispenseId() const
{
    return d->dispense_id;
}

QString Dispenser::dispenseDate() const
{
    return d->dispense_date;
}

void Dispenser::setDispenseDate(const QString &dispenseDate)
{
    d->dispense_date = dispenseDate;
}

int Dispenser::patient() const
{
    return d->patient;
}

void Dispenser::setPatient(int patient)
{
    d->patient = patient;
}

QString Dispenser::ailment() const
{
    return d->ailment;
}

void Dispenser::setAilment(const QString &ailment)
{
    d->ailment = ailment;
}

int Dispenser::drugDispensed() const
{
    return d->drug_dispensed;
}

void Dispenser::setDrugDispensed(int drugDispensed)
{
    d->drug_dispensed = drugDispensed;
}

QString Dispenser::dosageGiven() const
{
    return d->dosage_given;
}

void Dispenser::setDosageGiven(const QString &dosageGiven)
{
    d->dosage_given = dosageGiven;
}

QString Dispenser::createdAt() const
{
    return d->created_at;
}

QString Dispenser::updatedAt() const
{
    return d->updated_at;
}

int Dispenser::lockRevision() const
{
    return d->lock_revision;
}

Dispenser &Dispenser::operator=(const Dispenser &other)
{
    d = other.d;  // increments the reference count of the data
    return *this;
}

Dispenser Dispenser::create(const QString &dispenseDate, int patient, const QString &ailment, int drugDispensed, const QString &dosageGiven)
{
    DispenserObject obj;
    obj.dispense_date = dispenseDate;
    obj.patient = patient;
    obj.ailment = ailment;
    obj.drug_dispensed = drugDispensed;
    obj.dosage_given = dosageGiven;
    if (!obj.create()) {
        return Dispenser();
    }
    return Dispenser(obj);
}

Dispenser Dispenser::create(const QVariantMap &values)
{
    Dispenser model;
    model.setProperties(values);
    if (!model.d->create()) {
        model.d->clear();
    }
    return model;
}

Dispenser Dispenser::get(int dispenseId)
{
    TSqlORMapper<DispenserObject> mapper;
    return Dispenser(mapper.findByPrimaryKey(dispenseId));
}

Dispenser Dispenser::get(int dispenseId, int lockRevision)
{
    TSqlORMapper<DispenserObject> mapper;
    TCriteria cri;
    cri.add(DispenserObject::DispenseId, dispenseId);
    cri.add(DispenserObject::LockRevision, lockRevision);
    return Dispenser(mapper.findFirst(cri));
}

int Dispenser::count()
{
    TSqlORMapper<DispenserObject> mapper;
    return mapper.findCount();
}

QList<Dispenser> Dispenser::getAll()
{
    return tfGetModelListByCriteria<Dispenser, DispenserObject>(TCriteria());
}

QJsonArray Dispenser::getAllJson()
{
    QJsonArray array;
    TSqlORMapper<DispenserObject> mapper;

    if (mapper.find() > 0) {
        for (TSqlORMapperIterator<DispenserObject> i(mapper); i.hasNext(); ) {
            array.append(QJsonValue(QJsonObject::fromVariantMap(Dispenser(i.next()).toVariantMap())));
        }
    }
    return array;
}

TModelObject *Dispenser::modelData()
{
    return d.data();
}

const TModelObject *Dispenser::modelData() const
{
    return d.data();
}

#ifndef PATIENT_H
#define PATIENT_H

#include <QStringList>
#include <QDateTime>
#include <QVariant>
#include <QSharedDataPointer>
#include <TGlobal>
#include <TAbstractModel>

class TModelObject;
class PatientObject;
class QJsonArray;


class T_MODEL_EXPORT Patient : public TAbstractModel
{
public:
    Patient();
    Patient(const Patient &other);
    Patient(const PatientObject &object);
    ~Patient();

    int patientId() const;
    QString patientLastname() const;
    void setPatientLastname(const QString &patientLastname);
    QString patientOthernames() const;
    void setPatientOthernames(const QString &patientOthernames);
    QString patientDob() const;
    void setPatientDob(const QString &patientDob);
    QString patientGender() const;
    void setPatientGender(const QString &patientGender);
    QString patientWeight() const;
    void setPatientWeight(const QString &patientWeight);
    int allergy() const;
    void setAllergy(int allergy);
    QString createdAt() const;
    QString updatedAt() const;
    int lockRevision() const;
    Patient &operator=(const Patient &other);

    bool create() { return TAbstractModel::create(); }
    bool update() { return TAbstractModel::update(); }
    bool save()   { return TAbstractModel::save(); }
    bool remove() { return TAbstractModel::remove(); }

    static Patient create(const QString &patientLastname, const QString &patientOthernames, const QString &patientDob, const QString &patientGender, const QString &patientWeight, int allergy);
    static Patient create(const QVariantMap &values);
    static Patient get(int patientId);
    static Patient get(int patientId, int lockRevision);
    static int count();
    static QList<Patient> getAll();
    static QJsonArray getAllJson();

private:
    QSharedDataPointer<PatientObject> d;

    TModelObject *modelData();
    const TModelObject *modelData() const;
};

Q_DECLARE_METATYPE(Patient)
Q_DECLARE_METATYPE(QList<Patient>)

#endif // PATIENT_H

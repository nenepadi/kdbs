#include <TreeFrogModel>
#include "allergytype.h"
#include "allergytypeobject.h"

AllergyType::AllergyType()
    : TAbstractModel(), d(new AllergyTypeObject)
{
    d->type_id = 0;
    d->lock_revision = 0;
}

AllergyType::AllergyType(const AllergyType &other)
    : TAbstractModel(), d(new AllergyTypeObject(*other.d))
{ }

AllergyType::AllergyType(const AllergyTypeObject &object)
    : TAbstractModel(), d(new AllergyTypeObject(object))
{ }

AllergyType::~AllergyType()
{
    // If the reference count becomes 0,
    // the shared data object 'AllergyTypeObject' is deleted.
}

int AllergyType::typeId() const
{
    return d->type_id;
}

QString AllergyType::typeName() const
{
    return d->type_name;
}

void AllergyType::setTypeName(const QString &typeName)
{
    d->type_name = typeName;
}

QString AllergyType::description() const
{
    return d->description;
}

void AllergyType::setDescription(const QString &description)
{
    d->description = description;
}

QString AllergyType::createdAt() const
{
    return d->created_at;
}

QString AllergyType::updatedAt() const
{
    return d->updated_at;
}

int AllergyType::lockRevision() const
{
    return d->lock_revision;
}

AllergyType &AllergyType::operator=(const AllergyType &other)
{
    d = other.d;  // increments the reference count of the data
    return *this;
}

AllergyType AllergyType::create(const QString &typeName, const QString &description)
{
    AllergyTypeObject obj;
    obj.type_name = typeName;
    obj.description = description;
    if (!obj.create()) {
        return AllergyType();
    }
    return AllergyType(obj);
}

AllergyType AllergyType::create(const QVariantMap &values)
{
    AllergyType model;
    model.setProperties(values);
    if (!model.d->create()) {
        model.d->clear();
    }
    return model;
}

AllergyType AllergyType::get(int typeId)
{
    TSqlORMapper<AllergyTypeObject> mapper;
    return AllergyType(mapper.findByPrimaryKey(typeId));
}

AllergyType AllergyType::get(int typeId, int lockRevision)
{
    TSqlORMapper<AllergyTypeObject> mapper;
    TCriteria cri;
    cri.add(AllergyTypeObject::TypeId, typeId);
    cri.add(AllergyTypeObject::LockRevision, lockRevision);
    return AllergyType(mapper.findFirst(cri));
}

int AllergyType::count()
{
    TSqlORMapper<AllergyTypeObject> mapper;
    return mapper.findCount();
}

QList<AllergyType> AllergyType::getAll()
{
    return tfGetModelListByCriteria<AllergyType, AllergyTypeObject>(TCriteria());
}

QJsonArray AllergyType::getAllJson()
{
    QJsonArray array;
    TSqlORMapper<AllergyTypeObject> mapper;

    if (mapper.find() > 0) {
        for (TSqlORMapperIterator<AllergyTypeObject> i(mapper); i.hasNext(); ) {
            array.append(QJsonValue(QJsonObject::fromVariantMap(AllergyType(i.next()).toVariantMap())));
        }
    }
    return array;
}

TModelObject *AllergyType::modelData()
{
    return d.data();
}

const TModelObject *AllergyType::modelData() const
{
    return d.data();
}

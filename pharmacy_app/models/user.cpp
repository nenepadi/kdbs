#include <TreeFrogModel>
#include "user.h"
#include "userobject.h"

User::User()
    : TAbstractUser(), TAbstractModel(), d(new UserObject)
{
    d->user_id = 0;
    d->role = 0;
    d->lock_revision = 0;
}

User::User(const User &other)
    : TAbstractUser(), TAbstractModel(), d(new UserObject(*other.d))
{ }

User::User(const UserObject &object)
    : TAbstractUser(), TAbstractModel(), d(new UserObject(object))
{ }


User::~User()
{
    // If the reference count becomes 0,
    // the shared data object 'UserObject' is deleted.
}

int User::userId() const
{
    return d->user_id;
}

QString User::userLastname() const
{
    return d->user_lastname;
}

void User::setUserLastname(const QString &userLastname)
{
    d->user_lastname = userLastname;
}

QString User::userOthernames() const
{
    return d->user_othernames;
}

void User::setUserOthernames(const QString &userOthernames)
{
    d->user_othernames = userOthernames;
}

QString User::username() const
{
    return d->username;
}

void User::setUsername(const QString &username)
{
    d->username = username;
}

QString User::password() const
{
    return d->password;
}

void User::setPassword(const QString &password)
{
    d->password = password;
}

int User::role() const
{
    return d->role;
}

void User::setRole(int role)
{
    d->role = role;
}

QString User::createdAt() const
{
    return d->created_at;
}

QString User::updatedAt() const
{
    return d->updated_at;
}

int User::lockRevision() const
{
    return d->lock_revision;
}

User &User::operator=(const User &other)
{
    d = other.d;  // increments the reference count of the data
    return *this;
}

User User::authenticate(const QString &username, const QString &password)
{
    if (username.isEmpty() || password.isEmpty())
        return User();

    TSqlORMapper<UserObject> mapper;
    UserObject obj = mapper.findFirst(TCriteria(UserObject::Username, username));
    if (obj.isNull() || obj.password != password) {
        obj.clear();
    }
    return User(obj);
}

User User::create(const QString &userLastname, const QString &userOthernames, const QString &username, const QString &password, int role)
{
    UserObject obj;
    obj.user_lastname = userLastname;
    obj.user_othernames = userOthernames;
    obj.username = username;
    obj.password = password;
    obj.role = role;
    if (!obj.create()) {
        return User();
    }
    return User(obj);
}

User User::create(const QVariantMap &values)
{
    User model;
    model.setProperties(values);
    if (!model.d->create()) {
        model.d->clear();
    }
    return model;
}

User User::get(int userId)
{
    TSqlORMapper<UserObject> mapper;
    return User(mapper.findByPrimaryKey(userId));
}

User User::get(int userId, int lockRevision)
{
    TSqlORMapper<UserObject> mapper;
    TCriteria cri;
    cri.add(UserObject::UserId, userId);
    cri.add(UserObject::LockRevision, lockRevision);
    return User(mapper.findFirst(cri));
}

int User::count()
{
    TSqlORMapper<UserObject> mapper;
    return mapper.findCount();
}

QList<User> User::getAll()
{
    return tfGetModelListByCriteria<User, UserObject>();
}

QJsonArray User::getAllJson()
{
    QJsonArray array;
    TSqlORMapper<UserObject> mapper;

    if (mapper.find() > 0) {
        for (TSqlORMapperIterator<UserObject> i(mapper); i.hasNext(); ) {
            array.append(QJsonValue(QJsonObject::fromVariantMap(User(i.next()).toVariantMap())));
        }
    }
    return array;
}

TModelObject *User::modelData()
{
    return d.data();
}

const TModelObject *User::modelData() const
{
    return d.data();
}

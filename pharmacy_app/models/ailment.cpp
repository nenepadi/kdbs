#include <TreeFrogModel>
#include "ailment.h"
#include "ailmentobject.h"

Ailment::Ailment()
    : TAbstractModel(), d(new AilmentObject)
{
    d->ailment_id = 0;
}

Ailment::Ailment(const Ailment &other)
    : TAbstractModel(), d(new AilmentObject(*other.d))
{ }

Ailment::Ailment(const AilmentObject &object)
    : TAbstractModel(), d(new AilmentObject(object))
{ }

Ailment::~Ailment()
{
    // If the reference count becomes 0,
    // the shared data object 'AilmentObject' is deleted.
}

int Ailment::ailmentId() const
{
    return d->ailment_id;
}

QString Ailment::name() const
{
    return d->name;
}

void Ailment::setName(const QString &name)
{
    d->name = name;
}

QString Ailment::symptoms() const
{
    return d->symptoms;
}

void Ailment::setSymptoms(const QString &symptoms)
{
    d->symptoms = symptoms;
}

Ailment &Ailment::operator=(const Ailment &other)
{
    d = other.d;  // increments the reference count of the data
    return *this;
}

Ailment Ailment::create(const QString &name, const QString &symptoms)
{
    AilmentObject obj;
    obj.name = name;
    obj.symptoms = symptoms;
    if (!obj.create()) {
        return Ailment();
    }
    return Ailment(obj);
}

Ailment Ailment::create(const QVariantMap &values)
{
    Ailment model;
    model.setProperties(values);
    if (!model.d->create()) {
        model.d->clear();
    }
    return model;
}

Ailment Ailment::get(int ailmentId)
{
    TSqlORMapper<AilmentObject> mapper;
    return Ailment(mapper.findByPrimaryKey(ailmentId));
}

int Ailment::count()
{
    TSqlORMapper<AilmentObject> mapper;
    return mapper.findCount();
}

QList<Ailment> Ailment::getAll()
{
    return tfGetModelListByCriteria<Ailment, AilmentObject>(TCriteria());
}

QJsonArray Ailment::getAllJson()
{
    QJsonArray array;
    TSqlORMapper<AilmentObject> mapper;

    if (mapper.find() > 0) {
        for (TSqlORMapperIterator<AilmentObject> i(mapper); i.hasNext(); ) {
            array.append(QJsonValue(QJsonObject::fromVariantMap(Ailment(i.next()).toVariantMap())));
        }
    }
    return array;
}

TModelObject *Ailment::modelData()
{
    return d.data();
}

const TModelObject *Ailment::modelData() const
{
    return d.data();
}

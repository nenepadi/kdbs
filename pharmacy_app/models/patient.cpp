#include <TreeFrogModel>
#include "patient.h"
#include "patientobject.h"

Patient::Patient()
    : TAbstractModel(), d(new PatientObject)
{
    d->patient_id = 0;
    d->allergy = 0;
    d->lock_revision = 0;
}

Patient::Patient(const Patient &other)
    : TAbstractModel(), d(new PatientObject(*other.d))
{ }

Patient::Patient(const PatientObject &object)
    : TAbstractModel(), d(new PatientObject(object))
{ }

Patient::~Patient()
{
    // If the reference count becomes 0,
    // the shared data object 'PatientObject' is deleted.
}

int Patient::patientId() const
{
    return d->patient_id;
}

QString Patient::patientLastname() const
{
    return d->patient_lastname;
}

void Patient::setPatientLastname(const QString &patientLastname)
{
    d->patient_lastname = patientLastname;
}

QString Patient::patientOthernames() const
{
    return d->patient_othernames;
}

void Patient::setPatientOthernames(const QString &patientOthernames)
{
    d->patient_othernames = patientOthernames;
}

QString Patient::patientDob() const
{
    return d->patient_dob;
}

void Patient::setPatientDob(const QString &patientDob)
{
    d->patient_dob = patientDob;
}

QString Patient::patientGender() const
{
    return d->patient_gender;
}

void Patient::setPatientGender(const QString &patientGender)
{
    d->patient_gender = patientGender;
}

QString Patient::patientWeight() const
{
    return d->patient_weight;
}

void Patient::setPatientWeight(const QString &patientWeight)
{
    d->patient_weight = patientWeight;
}

int Patient::allergy() const
{
    return d->allergy;
}

void Patient::setAllergy(int allergy)
{
    d->allergy = allergy;
}

QString Patient::createdAt() const
{
    return d->created_at;
}

QString Patient::updatedAt() const
{
    return d->updated_at;
}

int Patient::lockRevision() const
{
    return d->lock_revision;
}

Patient &Patient::operator=(const Patient &other)
{
    d = other.d;  // increments the reference count of the data
    return *this;
}

Patient Patient::create(const QString &patientLastname, const QString &patientOthernames, const QString &patientDob, const QString &patientGender, const QString &patientWeight, int allergy)
{
    PatientObject obj;
    obj.patient_lastname = patientLastname;
    obj.patient_othernames = patientOthernames;
    obj.patient_dob = patientDob;
    obj.patient_gender = patientGender;
    obj.patient_weight = patientWeight;
    obj.allergy = allergy;
    if (!obj.create()) {
        return Patient();
    }
    return Patient(obj);
}

Patient Patient::create(const QVariantMap &values)
{
    Patient model;
    model.setProperties(values);
    if (!model.d->create()) {
        model.d->clear();
    }
    return model;
}

Patient Patient::get(int patientId)
{
    TSqlORMapper<PatientObject> mapper;
    return Patient(mapper.findByPrimaryKey(patientId));
}

Patient Patient::get(int patientId, int lockRevision)
{
    TSqlORMapper<PatientObject> mapper;
    TCriteria cri;
    cri.add(PatientObject::PatientId, patientId);
    cri.add(PatientObject::LockRevision, lockRevision);
    return Patient(mapper.findFirst(cri));
}

int Patient::count()
{
    TSqlORMapper<PatientObject> mapper;
    return mapper.findCount();
}

QList<Patient> Patient::getAll()
{
    return tfGetModelListByCriteria<Patient, PatientObject>(TCriteria());
}

QJsonArray Patient::getAllJson()
{
    QJsonArray array;
    TSqlORMapper<PatientObject> mapper;

    if (mapper.find() > 0) {
        for (TSqlORMapperIterator<PatientObject> i(mapper); i.hasNext(); ) {
            array.append(QJsonValue(QJsonObject::fromVariantMap(Patient(i.next()).toVariantMap())));
        }
    }
    return array;
}

TModelObject *Patient::modelData()
{
    return d.data();
}

const TModelObject *Patient::modelData() const
{
    return d.data();
}

TARGET = model
TEMPLATE = lib
CONFIG += shared
QT += sql
QT -= gui
DEFINES += TF_DLL
DESTDIR = ../lib
INCLUDEPATH += ../helpers sqlobjects mongoobjects
DEPENDPATH  += ../helpers sqlobjects mongoobjects
LIBS += -L../lib -lhelper

include(../appbase.pri)

HEADERS += sqlobjects/userobject.h
HEADERS += user.h
SOURCES += user.cpp
HEADERS += sqlobjects/patientobject.h
HEADERS += patient.h
SOURCES += patient.cpp
HEADERS += sqlobjects/drugobject.h
HEADERS += drug.h
SOURCES += drug.cpp
HEADERS += sqlobjects/dispenserobject.h
HEADERS += dispenser.h
SOURCES += dispenser.cpp
HEADERS += sqlobjects/allergyobject.h
HEADERS += allergy.h
SOURCES += allergy.cpp
HEADERS += sqlobjects/allergytypeobject.h
HEADERS += allergytype.h
SOURCES += allergytype.cpp
HEADERS += sqlobjects/roleobject.h
HEADERS += role.h
SOURCES += role.cpp
HEADERS += sqlobjects/ailmentobject.h
HEADERS += ailment.h
SOURCES += ailment.cpp

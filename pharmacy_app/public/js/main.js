function getAge(dateString) {
    var today = new Date();
    var birthDate = new Date(dateString);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())){
        age--;
    }
    return age;
}

//fix detailing ...
function OnDropDownChange(dropDown){
    var selectedValue = dropDown.options[dropDown.selectedIndex].value;
    $.when(
        $.get('/Dispenser/getPatientParticulars'),
        $.get('/Dispenser/getAllergies')
    ).done(function(obj1, obj2){
        $.each(obj1[0], function(idx1, val1){
            if(selectedValue == obj1[0][idx1].patientId){
                $('#weight').html(obj1[0][idx1].patientWeight + "kg.");
                $('#age').html(getAge(obj1[0][idx1].patientDob) + "yrs.");
                if(obj1[0][idx1].allergy !== null){
                    $.each(obj2[0], function(idx2, val2){
                        if(obj1[0][idx1].allergy == obj2[0][idx2].allergyId){
                            $('#allergy').html(obj2[0][idx2].allergyName);
                            $('#allergy_details').html(obj2[0][idx2].description);
                        }
                    });
                }
            }
        });
    });
}
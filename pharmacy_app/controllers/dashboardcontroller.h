#ifndef DASHBOARDCONTROLLER_H
#define DASHBOARDCONTROLLER_H

#include "applicationcontroller.h"
#include "user.h"
#include <iostream>


class T_CONTROLLER_EXPORT DashboardController : public ApplicationController
{
    Q_OBJECT
public:
    DashboardController() { }
    DashboardController(const DashboardController &other);

public slots:
    void index();

protected:
    bool preFilter();
};

T_DECLARE_CONTROLLER(DashboardController, dashboardcontroller)

#endif // DASHBOARDCONTROLLER_H

#ifndef PATIENTCONTROLLER_H
#define PATIENTCONTROLLER_H

#include "applicationcontroller.h"
#include "user.h"
#include "allergy.h"
#include "dispenser.h"
#include "drug.h"

class T_CONTROLLER_EXPORT PatientController : public ApplicationController
{
    Q_OBJECT
public:
    PatientController() { }
    PatientController(const PatientController &other);

public slots:
    void index();
    void show(const QString &pk);
    void entry();
    void create();
    void edit(const QString &pk);
    void save(const QString &pk);
    void remove(const QString &pk);

private:
    void renderEntry(const QVariantMap &patient = QVariantMap());
    void renderEdit(const QVariantMap &patient = QVariantMap());

protected:
    bool preFilter();
};

T_DECLARE_CONTROLLER(PatientController, patientcontroller)

#endif // PATIENTCONTROLLER_H

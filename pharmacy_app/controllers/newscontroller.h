#ifndef NEWSCONTROLLER_H
#define NEWSCONTROLLER_H

#include "applicationcontroller.h"
#include "allergy.h"
#include "allergytype.h"
#include "drug.h"


class T_CONTROLLER_EXPORT NewsController : public ApplicationController
{
    Q_OBJECT
public:
    NewsController() { }
    NewsController(const NewsController &other);

public slots:
    void index();
};

T_DECLARE_CONTROLLER(NewsController, newscontroller)

#endif // NEWSCONTROLLER_H

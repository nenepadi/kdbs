#include "patientcontroller.h"
#include "patient.h"


PatientController::PatientController(const PatientController &)
    : ApplicationController()
{ }

void PatientController::index(){
    QList<Patient> patientList = Patient::getAll();
    texport(patientList);

    QList<Allergy> allergy_pull = Allergy::getAll();
    texport(allergy_pull);
    render();
}

void PatientController::show(const QString &pk){
    Patient patient = Patient::get(pk.toInt());
    texport(patient);

    QList<Dispenser> histories = Dispenser::getAll();
    texport(histories);

    QList<Drug> drugs = Drug::getAll();
    texport(drugs);

    QList<Allergy> allergies = Allergy::getAll();
    texport(allergies);

    int count = 1;
    texport(count);

    render();
}

void PatientController::entry(){
    QList<Allergy> allergy_pull = Allergy::getAll();
    texport(allergy_pull);

    renderEntry();
}

void PatientController::create(){
    if (httpRequest().method() != Tf::Post) {
        return;
    }

    QVariantMap form = httpRequest().formItems("patient");
    Patient patient = Patient::create(form);
    if (!patient.isNull()) {
        QString notice = "Created successfully.";
        tflash(notice);
        redirect(urla("show", patient.patientId()));
    } else {
        QString error = "Failed to create.";
        texport(error);
        renderEntry(form);
    }
}

void PatientController::renderEntry(const QVariantMap &patient){
    texport(patient);
    render("entry");
}

void PatientController::edit(const QString &pk){
    QString loginUser = identityKeyOfLoginUser();
    texport(loginUser);

    Patient patient = Patient::get(pk.toInt());
    if (!patient.isNull()) {
        session().insert("patient_lockRevision", patient.lockRevision());
        QList<Allergy> allergy_pull = Allergy::getAll();
        texport(allergy_pull);
        renderEdit(patient.toVariantMap());
    } else {
        redirect(urla("entry"));
    }
}

void PatientController::save(const QString &pk){
    if (httpRequest().method() != Tf::Post) {
        return;
    }

    QString error;
    int rev = session().value("patient_lockRevision").toInt();
    Patient patient = Patient::get(pk.toInt(), rev);
    if (patient.isNull()) {
        error = "Original data not found. It may have been updated/removed by another transaction.";
        tflash(error);
        redirect(urla("edit", pk));
        return;
    }

    QVariantMap form = httpRequest().formItems("patient");
    patient.setProperties(form);
    if (patient.save()) {
        QString notice = "Updated successfully.";
        tflash(notice);
        redirect(urla("show", pk));
    } else {
        error = "Failed to update.";
        texport(error);
        renderEdit(form);
    }
}

void PatientController::renderEdit(const QVariantMap &patient){
    texport(patient);
    render("edit");
}

void PatientController::remove(const QString &pk){
    if (httpRequest().method() != Tf::Post) {
        return;
    }

    Patient patient = Patient::get(pk.toInt());
    patient.remove();
    redirect(urla("index"));
}

bool PatientController::preFilter(){
    User logged_user = User::get(getLoggedUser());
    if(!isUserLoggedIn()){
        redirect(url("Account", "userLoginForm"));
        return false;
    }

    texport(logged_user);

    return true;
}


// Don't remove below this line
T_REGISTER_CONTROLLER(patientcontroller)

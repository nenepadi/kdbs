#ifndef DRUGCONTROLLER_H
#define DRUGCONTROLLER_H

#include "applicationcontroller.h"
#include "drug.h"
#include "user.h"
#include "ailment.h"


class T_CONTROLLER_EXPORT DrugController : public ApplicationController
{
    Q_OBJECT
public:
    DrugController() { }
    DrugController(const DrugController &other);

public slots:
    void index();
    void show(const QString &pk);
    void entry();
    void create();
    void edit(const QString &pk);
    void save(const QString &pk);
    void remove(const QString &pk);

private:
    void renderEntry(const QVariantMap &drug = QVariantMap());
    void renderEdit(const QVariantMap &drug = QVariantMap());

protected:
    bool preFilter();
    void setAccessRules();
};

T_DECLARE_CONTROLLER(DrugController, drugcontroller)

#endif // DRUGCONTROLLER_H

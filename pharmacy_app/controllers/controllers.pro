TARGET = controller
TEMPLATE = lib
CONFIG += shared
QT += network sql xml
QT -= gui
DEFINES += TF_DLL
DESTDIR = ../lib
INCLUDEPATH += ../helpers ../models
DEPENDPATH  += ../helpers ../models
LIBS += -L../lib -lhelper -lmodel

include(../appbase.pri)

HEADERS += applicationcontroller.h
SOURCES += applicationcontroller.cpp
HEADERS += accountcontroller.h
SOURCES += accountcontroller.cpp
HEADERS += dashboardcontroller.h
SOURCES += dashboardcontroller.cpp
HEADERS += dispensercontroller.h
SOURCES += dispensercontroller.cpp
HEADERS += patientcontroller.h
SOURCES += patientcontroller.cpp
HEADERS += usercontroller.h
SOURCES += usercontroller.cpp
HEADERS += drugcontroller.h
SOURCES += drugcontroller.cpp
HEADERS += newscontroller.h
SOURCES += newscontroller.cpp

#include "drugcontroller.h"


DrugController::DrugController(const DrugController &)
    : ApplicationController()
{ }

void DrugController::index(){
    QList<Drug> drugList = Drug::getAll();
    texport(drugList);
    render();
}

void DrugController::entry(){
    QList<Ailment> ailments = Ailment::getAll();
    texport(ailments);
    renderEntry();
}

void DrugController::create(){
    if (httpRequest().method() != Tf::Post) {
        return;
    }

    QVariantMap form = httpRequest().formItems("drug");
    Drug drug = Drug::create(form);
    if (!drug.isNull()) {
        QString notice = "Created successfully.";
        tflash(notice);
        redirect(urla("index"));
    } else {
        QString error = "Failed to create.";
        texport(error);
        renderEntry(form);
    }
}

void DrugController::renderEntry(const QVariantMap &drug){
    texport(drug);
    render("entry");
}

void DrugController::show(const QString &pk){
    QString loginUser = identityKeyOfLoginUser();
    texport(loginUser);

    Drug drug = Drug::get(pk.toInt());
    texport(drug);
    render();
}

void DrugController::edit(const QString &pk){
    QString loginUser = identityKeyOfLoginUser();
    texport(loginUser);

    QList<Ailment> ailments = Ailment::getAll();
    texport(ailments);

    Drug drug = Drug::get(pk.toInt());
    if (!drug.isNull()) {
        session().insert("drug_lockRevision", drug.lockRevision());
        renderEdit(drug.toVariantMap());
    } else {
        redirect(urla("entry"));
    }
}

void DrugController::renderEdit(const QVariantMap &drug){
    texport(drug);
    render("edit");
}

void DrugController::save(const QString &pk){
    if (httpRequest().method() != Tf::Post) {
        return;
    }

    QString error;
    int rev = session().value("drug_lockRevision").toInt();
    Drug drug = Drug::get(pk.toInt(), rev);
    if (drug.isNull()) {
        error = "Original data not found. It may have been updated/removed by another transaction.";
        tflash(error);
        redirect(urla("edit", pk));
        return;
    }

    QVariantMap form = httpRequest().formItems("drug");
    drug.setProperties(form);
    if (drug.save()) {
        QString notice = "Updated successfully.";
        tflash(notice);
        redirect(urla("index"));
    } else {
        error = "Failed to update.";
        texport(error);
        renderEdit(form);
    }
}

void DrugController::remove(const QString &pk){
    if (httpRequest().method() != Tf::Post) {
        return;
    }

    Drug drug = Drug::get(pk.toInt());
    drug.remove();
    redirect(urla("index"));
}

void DrugController::setAccessRules(){
    User pharm = User::get(getLoggedUser());
    setDenyDefault(true);
    QStringList actions;
    actions << "index" << "show" << "entry" << "create" << "edit" << "save" << "remove";
    if(pharm.role() == 2){
        setAllowUser(identityKeyOfLoginUser(), actions);
    } else{
        setDenyUser(identityKeyOfLoginUser(), actions);
    }
}

bool DrugController::preFilter(){
    User logged_user = User::get(getLoggedUser());

    if (!isUserLoggedIn()) {
        redirect(url("Account", "userLoginForm"));
        return false;
    } else if(!validateAccess(&logged_user)) {
        renderErrorResponse(403);
        return false;
    }

    texport(logged_user);

    return true;
}

// Don't remove below this line
T_REGISTER_CONTROLLER(drugcontroller)

#include "applicationcontroller.h"


ApplicationController::ApplicationController()
    : TActionController()
{ }

ApplicationController::ApplicationController(const ApplicationController &)
    : TActionController()
{ }

ApplicationController::~ApplicationController()
{ }

void ApplicationController::staticInitialize()
{ }

void ApplicationController::staticRelease()
{ }

int ApplicationController::getLoggedUser(){
    QString login_name = identityKeyOfLoginUser();
    int iden;
    QList<User> allUsers = User::getAll();
    foreach(User user, allUsers){
        if(user.username() == login_name){
            iden = user.userId();
            break;
        }
    }
    return iden;
}

bool ApplicationController::preFilter()
{
    return true;
}


// Don't remove below this line
T_REGISTER_CONTROLLER(applicationcontroller)

#include "newscontroller.h"


NewsController::NewsController(const NewsController &)
    : ApplicationController()
{ }

void NewsController::index(){
    QList<Allergy> allergies = Allergy::getAll();
    texport(allergies);

    QList<AllergyType> allergyTypes = AllergyType::getAll();
    texport(allergyTypes);

    QList<Drug> drugs = Drug::getAll();
    texport(drugs);

    render();
}


// Don't remove below this line
T_REGISTER_CONTROLLER(newscontroller)

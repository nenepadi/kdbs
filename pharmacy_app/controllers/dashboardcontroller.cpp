#include "dashboardcontroller.h"


DashboardController::DashboardController(const DashboardController &) : ApplicationController(){ }

void DashboardController::index(){
    QString loginUser = identityKeyOfLoginUser();
    texport(loginUser);
    render();
}

bool DashboardController::preFilter(){
    User logged_user = User::get(getLoggedUser());

    if(!isUserLoggedIn()){
        redirect(url("Account", "userLoginForm"));
        return false;
    }

    texport(logged_user);
    return true;
}


// Don't remove below this line
T_REGISTER_CONTROLLER(dashboardcontroller)

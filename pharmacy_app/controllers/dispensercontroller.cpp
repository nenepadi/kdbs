#include "dispensercontroller.h"

DispenserController::DispenserController(const DispenserController &)
    : ApplicationController()
{ }

void DispenserController::index(){
    QList<Patient> patients = Patient::getAll();
    texport(patients);

    QList<Ailment> ailments = Ailment::getAll();
    texport(ailments);

    QList<Drug> drugs = Drug::getAll();
    texport(drugs);

    render("index");
}

void DispenserController::indexer(const QString &patId){
    Patient patient = Patient::get(patId.toInt());
    texport(patient);

    QList<Ailment> ailments = Ailment::getAll();
    texport(ailments);

    QList<Drug> drugs = Drug::getAll();
    texport(drugs);

    renderIndexer();
}

void DispenserController::renderIndexer(const QVariantMap &disPen){
    texport(disPen);
    render("indexer");
}

void DispenserController::create(){
    if (httpRequest().method() != Tf::Post) {
        return;
    }

    QString dispenseDate = QDateTime::currentDateTimeUtc().toString();
    QVariantMap form = httpRequest().formItems("disPen");

    QList<Patient> allPats = Patient::getAll();
    foreach(Patient pat, allPats){
        if(form["patient"] == pat.patientOthernames() + " " + pat.patientLastname()){
            form["patient"] = pat.patientId();
            break;
        }
    }

    Dispenser dispenser = Dispenser::create(dispenseDate,form["patient"].toInt(),
            form["ailment"].toString(),form["drugDispensed"].toInt(),form["dosageGiven"].toString());
    if (!dispenser.isNull()) {
        QString notice = "Created successfully.";
        tflash(notice);
        redirect(urla("viewReceipt", form["patient"].toString()));
    } else {
        QString error = "Failed to create.";
        texport(error);
        renderIndexer(form);
    }
}

void DispenserController::dispenseDrug(){
    if (httpRequest().method() != Tf::Post) {
        return;
    }

    QString dispenseDate = QDateTime::currentDateTimeUtc().toString();
    QString patient = httpRequest().formItemValue("patient");
    QString ailment = httpRequest().formItemValue("ailment");
    int drug = httpRequest().formItemValue("drugDispensed").toInt();
    QString dosage = httpRequest().formItemValue("dosageGiven");

    Dispenser dispenser = Dispenser::create(dispenseDate,patient.toInt(),ailment,drug,dosage);
    if (!dispenser.isNull()) {
        QString notice = "Created successfully.";
        tflash(notice);
        redirect(urla("viewReceipt", patient));
    } else {
        QString error = "Failed to create.";
        texport(error);
        redirect(urla("index"));
    }
}

void DispenserController::viewReceipt(const QString &patId){
    QDateTime curr = QDateTime::currentDateTimeUtc();
    QList<Dispenser> recentDispense = Dispenser::getAll();
    QList<Dispenser> pat_recentDispense;
    foreach (Dispenser disPen, recentDispense) {
        QDateTime checker = QDateTime::fromString(disPen.dispenseDate());
        if(patId.toInt() == disPen.patient() && checker <= curr && checker > curr.addSecs(-1000)){
            pat_recentDispense.append(disPen);
        }
    }
    texport(pat_recentDispense);

    Patient pat = Patient::get(patId.toInt());
    texport(pat);

    QList<Drug> drugs = Drug::getAll();
    texport(drugs);

    QList<Role> roles = Role::getAll();
    texport(roles);

    QList<Allergy> allergies = Allergy::getAll();
    texport(allergies);

    int count = 1;
    texport(count);

    render();
}

bool DispenserController::preFilter(){
    User logged_user = User::get(getLoggedUser());
    if(!isUserLoggedIn()){
        redirect(url("Account", "userLoginForm"));
        return false;
    }

    texport(logged_user);

    return true;
}


// Don't remove below this line
T_REGISTER_CONTROLLER(dispensercontroller)

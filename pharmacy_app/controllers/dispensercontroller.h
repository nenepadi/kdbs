#ifndef DISPENSERCONTROLLER_H
#define DISPENSERCONTROLLER_H

#include "applicationcontroller.h"
#include "user.h"
#include "patient.h"
#include "allergy.h"
#include "drug.h"
#include "dispenser.h"
#include "role.h"
#include "ailment.h"

class T_CONTROLLER_EXPORT DispenserController : public ApplicationController
{
    Q_OBJECT
public:
    DispenserController() { }
    DispenserController(const DispenserController &other);

public slots:
    void index();
    void create();
    void dispenseDrug();
    void viewReceipt(const QString &pk);
    void indexer(const QString &patId);

private:
    void renderIndexer(const QVariantMap &disPen = QVariantMap());

protected:
    bool preFilter();
};

T_DECLARE_CONTROLLER(DispenserController, dispensercontroller)

#endif // DISPENSERCONTROLLER_H

#include "usercontroller.h"


UserController::UserController(const UserController &)
    : ApplicationController()
{ }

void UserController::index(){
    QList<User> users = User::getAll();
    texport(users);

    QList<Role> roles = Role::getAll();
    texport(roles);
    render();
}

void UserController::show(const QString &pk){
    User user = User::get(pk.toInt());
    texport(user);
    render();
}

void UserController::entry(){
    QList<Role> roles = Role::getAll();
    texport(roles);

    renderEntry();
}

void UserController::create(){
    if (httpRequest().method() != Tf::Post) {
        return;
    }

    QVariantMap form = httpRequest().formItems("user");
    User user = User::create(form);
    if (!user.isNull()) {
        QString notice = "Created successfully.";
        tflash(notice);
        redirect(urla("show", user.userId()));
    } else {
        QString error = "Failed to create.";
        texport(error);
        renderEntry(form);
    }
}

void UserController::renderEntry(const QVariantMap &user){
    texport(user);
    render("entry");
}

void UserController::edit(const QString &pk){
    User user = User::get(pk.toInt());
    if (!user.isNull()) {
        session().insert("user_lockRevision", user.lockRevision());
        QList<Role> roles = Role::getAll();
        texport(roles);
        renderEdit(user.toVariantMap());
    } else {
        redirect(urla("entry"));
    }
}

void UserController::save(const QString &pk){
    if (httpRequest().method() != Tf::Post) {
        return;
    }

    QString error;
    int rev = session().value("user_lockRevision").toInt();
    User user = User::get(pk.toInt(), rev);
    if (user.isNull()) {
        error = "Original data not found. It may have been updated/removed by another transaction.";
        tflash(error);
        redirect(urla("edit", pk));
        return;
    }

    QVariantMap form = httpRequest().formItems("user");
    user.setProperties(form);
    if (user.save()) {
        QString notice = "Updated successfully.";
        tflash(notice);
        redirect(urla("show", pk));
    } else {
        error = "Failed to update.";
        texport(error);
        renderEdit(form);
    }
}

void UserController::renderEdit(const QVariantMap &user){
    texport(user);
    render("edit");
}

void UserController::remove(const QString &pk){
    if (httpRequest().method() != Tf::Post) {
        return;
    }

    User user = User::get(pk.toInt());
    user.remove();
    redirect(urla("index"));
}

void UserController::setAccessRules(){
    User admin = User::get(getLoggedUser());
    setDenyDefault(true);
    QStringList actions;
    actions << "index" << "show" << "entry" << "create" << "edit" << "save" << "remove";
    if(admin.role() == 1){
        setAllowUser(identityKeyOfLoginUser(), actions);
    } else{
        setDenyUser(identityKeyOfLoginUser(), actions);
    }
}

bool UserController::preFilter(){
    User logged_user = User::get(getLoggedUser());

    if (!isUserLoggedIn()) {
        redirect(url("Account", "userLoginForm"));
        return false;
    } else if(!validateAccess(&logged_user)) {
        renderErrorResponse(403);
        return false;
    }

    texport(logged_user);

    return true;
}


// Don't remove below this line
T_REGISTER_CONTROLLER(usercontroller)

#include "accountcontroller.h"


AccountController::AccountController(const AccountController &) : ApplicationController(){ }

void AccountController::userLoginForm(){
    setLayoutDisabled(true);
    userLogout();
    render("signin");
}

void AccountController::userSignin(){
    QString username = httpRequest().formItemValue("username");
    QString password = httpRequest().formItemValue("password");

    User user = User::authenticate(username, password);
    if(!user.isNull()){
        userLogin(&user);
        redirect(QUrl("/Dashboard"));
    } else{
        QString message = "Login failed";
        texport(message);
        render("signin");
    }
}

void AccountController::userSignout(){
   userLogout();
   redirect(QUrl("/Account/userLoginForm"));
}

void AccountController::userPasswdReset(){
    // write codes
}


// Don't remove below this line
T_REGISTER_CONTROLLER(accountcontroller)

#ifndef ACCOUNTCONTROLLER_H
#define ACCOUNTCONTROLLER_H

#include "applicationcontroller.h"
#include "user.h"


class T_CONTROLLER_EXPORT AccountController : public ApplicationController
{
    Q_OBJECT
public:
    AccountController() { }
    AccountController(const AccountController &other);

private slots:

public slots:
    void userLoginForm();
    void userSignin();
    void userSignout();
    void userPasswdReset();
};

T_DECLARE_CONTROLLER(AccountController, accountcontroller)

#endif // ACCOUNTCONTROLLER_H

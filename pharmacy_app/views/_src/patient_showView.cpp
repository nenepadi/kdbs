#include <QtCore>
#include <TreeFrogView>
#include "patient.h" 
#include "dispenser.h" 
#include "drug.h" 
#include "allergy.h" 
#include "applicationhelper.h"

class T_VIEW_EXPORT patient_showView : public TActionView
{
  Q_OBJECT
public:
  patient_showView() : TActionView() { }
  patient_showView(const patient_showView &) : TActionView() { }
  QString toString();
};

QString patient_showView::toString()
{
  responsebody.reserve(3764);
          tfetch(Patient, patient);
  tfetch(QList<Allergy>, allergies);
  tfetch(QList<Drug>, drugs);
  tfetch(QList<Dispenser>, histories);
  tfetch(int, count);
  responsebody += tr("\n<div class=\"jumbotron\">\n    <h1>");
  responsebody += THttpUtility::htmlEscape(patient.patientOthernames());
  responsebody += tr("&nbsp;");
  responsebody += THttpUtility::htmlEscape(patient.patientLastname());
  responsebody += tr("</h1>\n    <p>\n        Date of Birth: ");
  responsebody += THttpUtility::htmlEscape(patient.patientDob());
  responsebody += tr("<br>\n        Patient's weight: ");
  responsebody += THttpUtility::htmlEscape(patient.patientWeight());
  responsebody += tr("kg<br>\n        ");
  foreach(Allergy allergy, allergies){;
  responsebody += tr("            ");
  if(allergy.allergyId() == patient.allergy()){;
  responsebody += tr("                Allergy if any: ");
  responsebody += THttpUtility::htmlEscape(allergy.allergyName());
  responsebody += tr("<br>\n            ");
  };
  responsebody += tr("        ");
  };
  responsebody += tr("    </p>\n    <p>");
  responsebody += QVariant(linkTo("Back", urla("index"), Tf::Get, "", a("class", "btn btn-primary btn-lg") | a("role", "button"))).toString();
  responsebody += tr("</p>\n</div>\n\n<div class='panel panel-default grid'>\n    <div class='panel-heading'>\n        <i class='icon-table icon-large'></i>\n        History\n        <div class='panel-tools'>\n            <div class='btn-group'>\n                <a class='btn' data-toggle='toolbar-tooltip' href='' title='Reload'>\n                    <i class='icon-refresh'></i>\n                </a>\n            </div>\n        </div>\n    </div>\n    <div class='panel-body filters'>\n        <div class='row'>\n            <div class='col-md-3 col-md-offset-9'>\n                <div class='input-group'>\n                    <input class='form-control' placeholder='Quick search...' type='search'>\n                    <span class='input-group-btn'>\n                        <button class='btn' type='button'>\n                            <i class='icon-search'></i>\n                        </button>\n                    </span>\n                </div>\n            </div>\n        </div>\n    </div>\n    <table class='table'>\n        <thead>\n            <tr>\n                <th>#</th>\n                <th>Dispensed date</th>\n                <th>Ailment</th>\n                <th>Drug prescribed</th>\n                <th>Dosage given</th>\n            </tr>\n        </thead>\n        <tbody>\n            ");
  foreach(Dispenser dispen, histories){;
  responsebody += tr("            <tr>\n                ");
  if(patient.patientId() == dispen.patient()){;
  responsebody += tr("                    <td>");
  responsebody += THttpUtility::htmlEscape(count++);
  responsebody += tr("</td>\n                    <td>");
  responsebody += THttpUtility::htmlEscape(dispen.dispenseDate());
  responsebody += tr("</td>\n                    <td>");
  responsebody += THttpUtility::htmlEscape(dispen.ailment());
  responsebody += tr("</td>\n                    ");
  foreach(Drug drug, drugs){;
  responsebody += tr("                        ");
  if(drug.drugId() == dispen.drugDispensed()){;
  responsebody += tr("                            <td>");
  responsebody += THttpUtility::htmlEscape(drug.brandName());
  responsebody += tr("</td>\n                        ");
  };
  responsebody += tr("                    ");
  };
  responsebody += tr("                    <td>");
  responsebody += THttpUtility::htmlEscape(dispen.dosageGiven());
  responsebody += tr("</td>\n                ");
  };
  responsebody += tr("            </tr>\n            ");
  };
  responsebody += tr("        </tbody>\n    </table>\n</div>\n");

  return responsebody;
}

Q_DECLARE_METATYPE(patient_showView)
T_REGISTER_VIEW(patient_showView)

#include "patient_showView.moc"

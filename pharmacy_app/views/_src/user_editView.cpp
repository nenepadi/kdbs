#include <QtCore>
#include <TreeFrogView>
#include "user.h" 
#include "role.h" 
#include "applicationhelper.h"

class T_VIEW_EXPORT user_editView : public TActionView
{
  Q_OBJECT
public:
  user_editView() : TActionView() { }
  user_editView(const user_editView &) : TActionView() { }
  QString toString();
};

QString user_editView::toString()
{
  responsebody.reserve(3421);
      tfetch(QVariantMap, user);
  tfetch(QList<Role>, roles);
  responsebody += tr("\n<div class='panel panel-default'>\n  	<div class='panel-heading'>\n       	<i class='icon-plus icon-large'></i>\n       	");
  responsebody += THttpUtility::htmlEscape(user["username"]);
  responsebody += tr("'s record\n   	</div>\n   	<div class='panel-body'>\n   		");
  responsebody += QVariant(formTag(urla("save", user["userId"]), Tf::Post, "", a("class", "form-horizontal"))).toString();
  responsebody += tr("\n   			<fieldset>\n   				<legend>Editing ...</legend>\n                <div class='form-group'>\n                  	<label class='col-lg-2 control-label'>Full name</label>\n                  	<div class='col-lg-5'>\n                    	<input name='user[userLastname]' class='form-control' value='");
  responsebody += THttpUtility::htmlEscape(user["userLastname"]);
  responsebody += tr("' \n                    	placeholder='Lastname' type='text' autofocus required>\n                  	</div>\n                  	<div class='col-lg-5'>\n                    	<input name='user[userOthernames]' class='form-control' value='");
  responsebody += THttpUtility::htmlEscape(user["userOthernames"]);
  responsebody += tr("' \n                    	placeholder='Other names' type='text' required>\n                  	</div>\n                </div>\n                <div class='form-group'>\n                  	<label class='col-lg-2 control-label'>Username</label>\n                  	<div class='col-lg-10'>\n                    	<input name='user[username]' class='form-control' value='");
  responsebody += THttpUtility::htmlEscape(user["username"]);
  responsebody += tr("' \n                    	placeholder='username' type='text' required readonly>\n                  	</div>\n                </div>\n                <div class='form-group'>\n                    <label class='col-lg-2 control-label'>Password</label>\n                    <div class='col-lg-10'>\n                        <input name='user[password]' class='form-control' value='");
  responsebody += THttpUtility::htmlEscape(user["password"]);
  responsebody += tr("' \n                        placeholder='Password' type='password' required readonly>\n                    </div>\n                </div>\n                <div class='form-group'>\n                  	<label class='col-lg-2 control-label'>Role</label>\n                  	<div class='radio'>\n                        ");
  foreach(Role role, roles){;
  responsebody += tr("                    		<div class='col-lg-2 col-lg-offset-1'>\n                    			<input name='user[role]' type='radio' value='");
  responsebody += THttpUtility::htmlEscape(role.roleId());
  responsebody += tr("' required\n                    			");
  if(user["role"] == role.roleId()){;
  responsebody += tr("                    				checked\n                    			");
  };
  responsebody += tr("                    			> ");
  responsebody += THttpUtility::htmlEscape(role.roleName());
  responsebody += tr("\n                    		</div>\n                    	");
  };
  responsebody += tr("                  	</div>\n                </div>\n           	</fieldset>\n            <div class='form-actions'>\n            	<button class='btn btn-default' type='submit'>Save</button>\n                <a class='btn' href='/User/index'>Cancel</a>\n            </div>\n        </form>\n    </div>\n</div>\n");

  return responsebody;
}

Q_DECLARE_METATYPE(user_editView)
T_REGISTER_VIEW(user_editView)

#include "user_editView.moc"

#include <QtCore>
#include <TreeFrogView>
#include "patient.h" 
#include "allergy.h" 
#include "applicationhelper.h"

class T_VIEW_EXPORT patient_indexView : public TActionView
{
  Q_OBJECT
public:
  patient_indexView() : TActionView() { }
  patient_indexView(const patient_indexView &) : TActionView() { }
  QString toString();
};

QString patient_indexView::toString()
{
  responsebody.reserve(3890);
      tfetch(QList<Allergy>, allergy_pull);
  tfetch(QList<Patient>, patientList);
  responsebody += tr("<div class='panel panel-default grid'>\n	<div class='panel-heading'>\n		<i class='icon-table icon-large'></i>\n		Patient records\n		<div class='panel-tools'>\n			<div class='btn-group'>\n                <a class='btn' data-toggle='toolbar-tooltip' href='' title='Reload'>\n                    <i class='icon-refresh'></i>\n                </a>\n           	</div>\n        </div>\n   	</div>\n    <div class='panel-body filters'>\n    	<div class='row'>\n    		<div class='col-md-4'>\n                <a class='btn btn-primary' href='/Patient/entry'>\n                  	<i class='icon-plus'></i>\n                  	New patient\n                </a>\n           	</div>\n           	<div class='col-md-3 col-md-offset-5'>\n               	<div class='input-group'>\n                  	<input class='form-control' placeholder='Quick search...' type='search'>\n                  	<span class='input-group-btn'>\n                    	<button class='btn' type='button'>\n                      		<i class='icon-search'></i>\n                    	</button>\n                  	</span>\n                </div>\n            </div>\n        </div>\n    </div>\n    <table class='table'>\n        <thead>\n            <tr>\n            	<th>#</th>\n            	<th>Full Name</th>\n    			<th>Date of Birth</th>\n    			<th>Gender</th>\n    			<th>Weight</th>\n    			<th>Allergy</th>\n                <th class='actions'>\n                  Actions\n                </th>\n            </tr>\n        </thead>\n        <tbody>\n        	");
  foreach(Patient pat, patientList){;
  responsebody += tr("            <tr>\n            	<td>");
  responsebody += THttpUtility::htmlEscape(pat.patientId());
  responsebody += tr("</td>\n                <td>\n                    <a href='/Patient/show/");
  responsebody += THttpUtility::htmlEscape(pat.patientId());
  responsebody += tr("'>\n                        ");
  responsebody += THttpUtility::htmlEscape(pat.patientLastname());
  responsebody += tr(", &nbsp;");
  responsebody += THttpUtility::htmlEscape(pat.patientOthernames());
  responsebody += tr("\n                    </a>\n                </td>\n                <td>");
  responsebody += THttpUtility::htmlEscape(pat.patientDob());
  responsebody += tr("</td>\n                <td>");
  responsebody += THttpUtility::htmlEscape(pat.patientGender());
  responsebody += tr("</td>\n                <td>");
  responsebody += THttpUtility::htmlEscape(pat.patientWeight());
  responsebody += tr("kg</td>\n                <td>\n                ");
  foreach(Allergy al, allergy_pull){
                	if(pat.allergy() == al.allergyId()){
                		eh(al.allergyName());
                	}
                };
  responsebody += tr("                </td>\n                <td class='action'>\n                  	<a class='btn btn-info' href='/Patient/edit/");
  responsebody += THttpUtility::htmlEscape(pat.patientId());
  responsebody += tr("' data-toggle='tooltip' title='Edit'>\n                    	<i class='icon-edit'></i>\n                  	</a>&nbsp;\n                    <a class='btn btn-success' href='/Dispenser/indexer/");
  responsebody += THttpUtility::htmlEscape(pat.patientId());
  responsebody += tr("' data-toggle='tooltip' title='Dispense drug'>\n                        <i class='icon-medkit'></i>\n                    </a>&nbsp;\n                  	");
  responsebody += QVariant(linkTo("<i class='icon-trash'></i>", urla("remove", pat.patientId()), Tf::Post, "confirm('Are you sure you want to delete this?')", a("class", "btn btn-danger") | a("data-toggle", "tooltip") | a("title", "Delete"))).toString();
  responsebody += tr("\n                </td>\n            </tr>\n            ");
  };
  responsebody += tr("        </tbody>\n    </table>\n</div>\n");

  return responsebody;
}

Q_DECLARE_METATYPE(patient_indexView)
T_REGISTER_VIEW(patient_indexView)

#include "patient_indexView.moc"

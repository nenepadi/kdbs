#include <QtCore>
#include <TreeFrogView>
#include "applicationhelper.h"

class T_VIEW_EXPORT account_signinView : public TActionView
{
  Q_OBJECT
public:
  account_signinView() : TActionView() { }
  account_signinView(const account_signinView &) : TActionView() { }
  QString toString();
};

QString account_signinView::toString()
{
  responsebody.reserve(2883);
  responsebody += tr("<!DOCTYPE html>\n<html lang=\"en\">\n	<head>\n		<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n  		<script type=\"text/javascript\">\n    			WEB_SOCKET_SWF_LOCATION = \"/__rack/WebSocketMain.swf\";\n  		</script>\n  		<script type=\"text/javascript\" src=\"/assets/swfobject.js\"></script>\n  		<script type=\"text/javascript\" src=\"/assets/web_socket.js\"></script>\n\n		<script type=\"text/javascript\">\n  			RACK_LIVERELOAD_PORT = 35729;\n		</script>\n		<script type=\"text/javascript\" src=\"/assets/livereload.js\"></script>\n\n    		<meta charset=\"utf-8\">\n    		<meta content=\"IE=edge,chrome=1\" http-equiv=\"X-UA-Compatible\">\n    		<meta content=\"\" name=\"description\">\n    		<meta content=\"\" name=\"keywords\">\n\n    		<!-- Title -->\n    		<title>Pharmacy Application - ");
  responsebody += THttpUtility::htmlEscape(controller()->name());
  responsebody += tr("</title>\n    \n    		<link href=\"/assets/application.css\" rel=\"stylesheet\" type=\"text/css\">\n    		<link href=\"/assets/font-awesome.min.css\" rel=\"stylesheet\" type=\"text/css\">   \n  	</head>\n  	<body class=\"login\">\n    		<div class=\"wrapper\">\n      			<div class=\"row\">\n        			<div class=\"col-lg-12\">\n          				<div class=\"brand text-center\">\n           	 				<h1>\n          						<div class=\"logo-icon\">\n            						<i class=\"icon-beer\"></i>\n          						</div>\n          						DrugDispense\n        					</h1>\n          				</div>\n        			</div>\n      			</div>\n      			\n      			<div class=\"row\">\n        			<div class=\"col-lg-12\">\n          				");
  responsebody += QVariant(formTag(urla("userSignin"))).toString();
  responsebody += tr("\n            					<fieldset class=\"text-center\">\n              						<legend>Login to your account</legend>\n              						<div class=\"form-group\">\n                						<input class=\"form-control\" placeholder=\"username\" type=\"text\" name=\"username\">\n              						</div>\n              						<div class=\"form-group\">\n                						<input class=\"form-control\" placeholder=\"password\" type=\"password\" name=\"password\">\n              						</div>\n              						<div class=\"text-center\">\n                						<button class=\"btn btn-default\">Sign in</button>\n              						</div>\n            					</fieldset>\n          				</form>\n        			</div>\n      			</div>\n    		</div>\n    \n    		<!-- Footer -->\n    		<!-- Javascripts -->\n    		<script src=\"/assets/jquery.min.js\" type=\"text/javascript\"></script>\n		<script src=\"/assets/jquery-ui.min.js\" type=\"text/javascript\"></script>\n		<script src=\"/assets/modernizr.min.js\" type=\"text/javascript\"></script>\n		<script src=\"/assets/application.js\" type=\"text/javascript\"></script>\n	</body>\n</html>");

  return responsebody;
}

Q_DECLARE_METATYPE(account_signinView)
T_REGISTER_VIEW(account_signinView)

#include "account_signinView.moc"

#include <QtCore>
#include <TreeFrogView>
#include "patient.h" 
#include "ailment.h" 
#include "drug.h" 
#include "applicationhelper.h"

class T_VIEW_EXPORT dispenser_indexView : public TActionView
{
  Q_OBJECT
public:
  dispenser_indexView() : TActionView() { }
  dispenser_indexView(const dispenser_indexView &) : TActionView() { }
  QString toString();
};

QString dispenser_indexView::toString()
{
  responsebody.reserve(8379);
        tfetch(QList<Patient>, patients);
  tfetch(QList<Ailment>, ailments);
  tfetch(QList<Drug>, drugs);
  responsebody += tr("<div class='row'>\n    <div class='col-lg-12'>\n        <div class='panel panel-default'>\n            <div class='panel-heading'>\n                <i class='icon-shopping-cart icon-large'></i>\n                Dispensary process\n            </div>\n            <div class='panel-body'>\n                ");
  responsebody += QVariant(formTag(urla("dispenseDrug"), Tf::Post, "", a("class", "form-horizontal"))).toString();
  responsebody += tr("\n                    <fieldset>\n                        <legend>Patient Background Information ...</legend>\n                        <div class='form-group'>\n                            <label for=\"patient\" class='col-lg-2 control-label'>Patient</label>\n                            <div class='col-lg-10'>\n                                <select name=\"patient\" class='form-control' required data-placeholder='Select a patient to administer drug to' id='patient'>\n                                    <option value=''></option>\n                                    ");
  foreach(Patient pat, patients){;
  responsebody += tr("                                    <option value='");
  responsebody += THttpUtility::htmlEscape(pat.patientId());
  responsebody += tr("'>\n                                    ");
  responsebody += THttpUtility::htmlEscape(pat.patientOthernames());
  responsebody += tr(" ");
  responsebody += THttpUtility::htmlEscape(pat.patientLastname());
  responsebody += tr("\n                                    </option>\n                                    ");
  };
  responsebody += tr("                                </select>\n                            </div>\n                        </div>\n                        <div class='form-group'>\n                            <label class='col-lg-2 control-label'>Ailment</label>\n                            <div class='col-lg-10'>\n                                <select name=\"ailment\" class='form-control' required data-placeholder='Search through the symptoms a select one' id='ailment'>\n                                    <option value=\"\"></option>\n                                    ");
  foreach(Ailment ail, ailments){;
  responsebody += tr("                                    <option value='");
  responsebody += THttpUtility::htmlEscape(ail.name());
  responsebody += tr("'>\n                                        ");
  responsebody += THttpUtility::htmlEscape(ail.symptoms());
  responsebody += tr("\n                                    </option>\n                                    ");
  };
  responsebody += tr("                                </select>\n                            </div>\n                        </div>\n                        <div class='form-group prescribe'>\n                            <label class='col-lg-2 control-label'>Prescription</label>\n                            <div class='col-lg-10'>\n                                <select class='form-control drug' name=\"drugDispensed\" required data-placeholder='Select a drug...'>\n                                    <option value=\"\"></option>\n                                    ");
  foreach(Drug drug, drugs){;
  responsebody += tr("                                    <option class='dgOpt' value='");
  responsebody += THttpUtility::htmlEscape(drug.drugId());
  responsebody += tr("' rel='");
  responsebody += THttpUtility::htmlEscape(drug.ailment());
  responsebody += tr("'>\n                                        ");
  responsebody += THttpUtility::htmlEscape(drug.brandName());
  responsebody += tr("\n                                    </option>\n                                    ");
  };
  responsebody += tr("                                </select>\n                            </div>\n                        </div>\n                        <div class='form-group prescribe'>\n                            <div class='col-lg-10 col-lg-offset-2'>\n                                <select class='form-control dose' name=\"dosageGiven\" required data-placeholder='Select dosage for drug...'>\n                                    <option value=\"\"></option>\n                                    ");
  foreach(Drug dg, drugs){;
  responsebody += tr("                                    <optgroup label=\"");
  responsebody += THttpUtility::htmlEscape(dg.brandName());
  responsebody += tr("\" rel=\"");
  responsebody += THttpUtility::htmlEscape(dg.drugId());
  responsebody += tr("\">\n                                        ");
  if(dg.dosage() != NULL){;
  responsebody += tr("                                        <option value=\"");
  responsebody += THttpUtility::htmlEscape(dg.dosage());
  responsebody += tr("\">General - ");
  responsebody += THttpUtility::htmlEscape(dg.dosage());
  responsebody += tr("</option>\n                                        ");
  };
  responsebody += tr("\n                                        ");
  if(dg.dosageChildren() != NULL){;
  responsebody += tr("                                        <option value=\"");
  responsebody += THttpUtility::htmlEscape(dg.dosageChildren());
  responsebody += tr("\">Children - ");
  responsebody += THttpUtility::htmlEscape(dg.dosageChildren());
  responsebody += tr("</option>\n                                        ");
  };
  responsebody += tr("\n                                        ");
  if(dg.dosageAdult() != NULL){;
  responsebody += tr("                                        <option value=\"");
  responsebody += THttpUtility::htmlEscape(dg.dosageAdult());
  responsebody += tr("\">Adult - ");
  responsebody += THttpUtility::htmlEscape(dg.dosageAdult());
  responsebody += tr("</option>\n                                        ");
  };
  responsebody += tr("                                    </optgroup>\n                                    ");
  };
  responsebody += tr("                                </select>\n                            </div>\n                        </div>\n                    </fieldset>\n                    <div class='form-actions'>\n                        <button id='save' class='btn btn-default' type='submit'>Save</button>&nbsp;\n                        <a class='btn' href='/Dashboard'>Cancel</a>\n                    </div>\n                </form>\n            </div>\n        </div>\n    </div>\n</div>\n\n<script type=\"text/javascript\">\n    $(function(){\n        $('#patient').chosen({\n            no_results_text: \"Please the patient hasn't been registered yet. Please make sure you register him/her <a href='/Patient/entry'>here</a>\"\n        });\n\n        $('#patient').on('chosen:no_results', function(evt, params){\n            var value = params.chosen.search_results.find('li.no-results').text();\n            var arr = value.split('\"');\n            alert(arr[0]);\n        });\n\n        $('#ailment').chosen({\n            no_results_text: \"Please advice the patient to see a doctor!!! There is no drug for his/her description\",\n            search_contains: true\n        });\n\n        $('select.drug').chosen({\n            disable_search_threshold: 5\n        });\n\n        $('select.dose').chosen({\n            disable_search_threshold: 5\n        });\n\n        $('#ailment').change(function(){\n            $('.dgOpt').removeAttr('disabled');\n            ail = $(this).val();\n            $('select.drug .dgOpt').each(function(){\n                if($(this).attr('rel') != ail){\n                    $(this).attr('disabled', 'disabled');\n                }\n            });\n            $('select.drug').trigger('chosen:updated');\n        });\n\n        $('#ailment').on('chosen:no_results', function(evt, params){\n            var value = params.chosen.search_results.find('li.no-results').text();\n            var arr = value.split('\"');\n            alert(arr[0]);\n        });\n\n        $('select.drug').change(function(){\n            $('select.dose optgroup').removeAttr('disabled');\n            drug = $(this).val();\n            $('select.dose optgroup').each(function(){\n                if($(this).attr('rel') != drug){\n                    $(this).attr('disabled', 'disabled');\n                }\n            });\n            $('select.dose').trigger('chosen:updated');\n        });\n    });\n</script>");

  return responsebody;
}

Q_DECLARE_METATYPE(dispenser_indexView)
T_REGISTER_VIEW(dispenser_indexView)

#include "dispenser_indexView.moc"

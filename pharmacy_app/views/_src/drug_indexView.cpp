#include <QtCore>
#include <TreeFrogView>
#include "drug.h" 
#include "applicationhelper.h"

class T_VIEW_EXPORT drug_indexView : public TActionView
{
  Q_OBJECT
public:
  drug_indexView() : TActionView() { }
  drug_indexView(const drug_indexView &) : TActionView() { }
  QString toString();
};

QString drug_indexView::toString()
{
  responsebody.reserve(2952);
    tfetch(QList<Drug>, drugList);
  responsebody += tr("<div class='panel panel-default grid'>\n	<div class='panel-heading'>\n		<i class='icon-table icon-large'></i>\n		Drugs in-stock\n		<div class='panel-tools'>\n			<div class='btn-group'>\n                <a class='btn' data-toggle='toolbar-tooltip' href='' title='Reload'>\n                  <i class='icon-refresh'></i>\n                </a>\n           	</div>\n        </div>\n   	</div>\n    <div class='panel-body filters'>\n    	<div class='row'>\n    		<div class='col-md-4'>\n                <a class='btn btn-primary' href='/Drug/entry'>\n                  	<i class='icon-plus'></i>\n                  	Add new drug\n                </a>\n           	</div>\n           	<div class='col-md-3 col-md-offset-5'>\n               	<div class='input-group'>\n                  	<input class='form-control' placeholder='Quick search...' type='search'>\n                  	<span class='input-group-btn'>\n                    	<button class='btn' type='button'>\n                      		<i class='icon-search'></i>\n                    	</button>\n                  	</span>\n                </div>\n            </div>\n        </div>\n    </div>\n    <table class='table'>\n        <thead>\n            <tr>\n            	<th>#</th>\n            	<th>Brand Name</th>\n            	<th>Ingredients</th>\n    			<th>Manufacturer</th>\n    			<th>Expiry date</th>\n                <th class='actions'>\n                  Actions\n                </th>\n            </tr>\n        </thead>\n        <tbody>\n        	");
  foreach(Drug drug, drugList){;
  responsebody += tr("            <tr>\n            	<td>");
  responsebody += THttpUtility::htmlEscape(drug.drugId());
  responsebody += tr("</td>\n                <td>");
  responsebody += THttpUtility::htmlEscape(drug.brandName());
  responsebody += tr("</td>\n                <td>");
  responsebody += THttpUtility::htmlEscape(drug.ingredients());
  responsebody += tr("</td>\n                <td>");
  responsebody += THttpUtility::htmlEscape(drug.manufacturer());
  responsebody += tr("</td>\n                <td>");
  responsebody += THttpUtility::htmlEscape(drug.expiryDate());
  responsebody += tr("</td>\n                <td class='action'>\n                  	<a class='btn btn-info' href='/Drug/edit/");
  responsebody += THttpUtility::htmlEscape(drug.drugId());
  responsebody += tr("' data-toggle='tooltip' title='Edit'>\n                    	<i class='icon-edit'></i>\n                  	</a>\n                  	");
  responsebody += QVariant(linkTo("<i class='icon-trash'></i>", urla("remove", drug.drugId()), Tf::Post, "confirm('Are you sure you want to delete this?')", a("class", "btn btn-danger") | a("data-toggle", "tooltip") | a("title", "Delete"))).toString();
  responsebody += tr("\n                </td>\n            </tr>\n            ");
  };
  responsebody += tr("        </tbody>\n    </table>\n    \n</div>\n");

  return responsebody;
}

Q_DECLARE_METATYPE(drug_indexView)
T_REGISTER_VIEW(drug_indexView)

#include "drug_indexView.moc"

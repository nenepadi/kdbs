#include <QtCore>
#include <TreeFrogView>
#include "user.h" 
#include "applicationhelper.h"

class T_VIEW_EXPORT layouts_applicationView : public TActionView
{
  Q_OBJECT
public:
  layouts_applicationView() : TActionView() { }
  layouts_applicationView(const layouts_applicationView &) : TActionView() { }
  QString toString();
};

QString layouts_applicationView::toString()
{
  responsebody.reserve(3687);
    tfetch(User, logged_user);
  responsebody += tr("<!DOCTYPE html>\n<html lang=\"en\">\n	<head>\n        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n        <meta content=\"IE=edge,chrome=1\" http-equiv=\"X-UA-Compatible\">\n        <meta content=\"\" name=\"description\">\n        <meta content=\"\" name=\"keywords\">\n\n        <link href=\"/assets/application.css\" rel=\"stylesheet\" type=\"text/css\">\n        <link href=\"/assets/font-awesome.min.css\" rel=\"stylesheet\" type=\"text/css\">\n        <link rel=\"stylesheet\" href=\"/css/chosen-bootstrap/chosen.bootstrap.css\">\n\n    	<script type=\"text/javascript\">\n       		WEB_SOCKET_SWF_LOCATION = \"/__rack/WebSocketMain.swf\";\n    	</script>\n   		<script type=\"text/javascript\" src=\"/assets/swfobject.js\"></script>\n    	<script type=\"text/javascript\" src=\"/assets/web_socket.js\"></script>\n    	<script type=\"text/javascript\">RACK_LIVERELOAD_PORT = 35729;</script>\n    	<script type=\"text/javascript\" src=\"/assets/livereload.js\"></script>\n    	<script src=\"/assets/jquery.min.js\" type=\"text/javascript\"></script>\n        <script src=\"/js/chosen/chosen.jquery.min.js\"></script>\n        <script src=\"/js/confirm-bootstrap.js\"></script>\n\n    	<!-- Title -->\n    	<title>Pharmacy Application - ");
  responsebody += THttpUtility::htmlEscape(controller()->name());
  responsebody += tr("</title>\n  	</head>\n\n  	<body class=\"main page\">\n   		<!-- start navbar -->\n    	<div class=\"navbar navbar-default\" id=\"navbar\">\n    		<a class=\"navbar-brand\" href=\"#\">\n      			<i class=\"icon-beer\"></i>\n      			DrugDispense\n    		</a>\n    		<ul class=\"nav navbar-nav pull-right\">\n				<li class=\"dropdown user\">\n					<a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\">\n						<i class=\"icon-user-md\"></i>\n						<strong>");
  responsebody += THttpUtility::htmlEscape(logged_user.username());
  responsebody += tr("</strong>\n						<img class=\"img-rounded\" src=\"/assets/777\">\n						<b class=\"caret\"></b>\n					</a>\n					<ul class=\"dropdown-menu\">\n						<li><a href=\"/Account/passwdReset\">Reset Password</a></li>\n						<li class=\"divider\"></li>\n						<li><a href=\"/Account/userSignout\">Sign out</a></li>\n					</ul>\n				</li>\n  			</ul>\n		</div>\n		<!-- end navbar -->\n\n		<div id=\"wrapper\">\n  			");
  responsebody += QVariant(renderPartial("navigation")).toString();
  responsebody += tr("\n\n  			<!-- start tools -->\n  			<section id=\"tools\">\n    			<ul class=\"breadcrumb\" id=\"breadcrumb\">\n      				<li class=\"title\">");
  responsebody += THttpUtility::htmlEscape(controller()->className());
  responsebody += tr("</li>\n      				<li class=\"active\"><a href=\"#\">");
  responsebody += THttpUtility::htmlEscape(controller()->activeAction());
  responsebody += tr("</a></li>\n    			</ul>\n    			<div id=\"toolbar\"></div>\n  			</section>\n  			<!-- end tools -->\n\n  			<!-- start content -->\n  			<div id=\"content\">\n    			");
  responsebody += QVariant(yield()).toString();
  responsebody += tr("\n  			</div>\n		</div>\n\n		<!-- Footer -->\n		<!-- Javascripts -->\n		<script src=\"/assets/jquery-ui.min.js\" type=\"text/javascript\"></script>\n		<script src=\"/assets/modernizr.min.js\" type=\"text/javascript\"></script>\n		<script src=\"/assets/application.js\" type=\"text/javascript\"></script>\n		<script src=\"/js/main.js\" type=\"text/javascript\"></script>\n\n		<script type=\"text/javascript\">\n			$(function(){\n				$('#dock li a[href^=\"/' + location.pathname.split(\"/\")[1] + '\"]').closest('li').addClass('active');\n			});\n		</script>\n	</body>\n</html>\n");

  return responsebody;
}

Q_DECLARE_METATYPE(layouts_applicationView)
T_REGISTER_VIEW(layouts_applicationView)

#include "layouts_applicationView.moc"

#include <QtCore>
#include <TreeFrogView>
#include "allergytype.h" 
#include "allergy.h" 
#include "drug.h" 
#include "applicationhelper.h"

class T_VIEW_EXPORT news_indexView : public TActionView
{
  Q_OBJECT
public:
  news_indexView() : TActionView() { }
  news_indexView(const news_indexView &) : TActionView() { }
  QString toString();
};

QString news_indexView::toString()
{
  responsebody.reserve(6502);
        tfetch(QList<AllergyType>, allergyTypes);
  tfetch(QList<Allergy>, allergies);
  tfetch(QList<Drug>, drugs);
  responsebody += tr("\n<div class='row'>\n    <div class='col-lg-12'>\n        <div class='panel panel-default'>\n            <div class='panel-heading'>\n                <i class='icon-info-sign icon-large'></i>\n                Detailed Information about drugs and allergies\n            </div>\n            <div class='panel-body'>\n                <ul class=\"nav nav-tabs\">\n                    <li><a href=\"#allergy\" data-toggle=\"tab\">Allergy Info</a></li>\n                    <li><a href=\"#drug\" data-toggle=\"tab\">Drug Info</a></li>\n                </ul>\n\n                <!-- Tab panes -->\n                <div class=\"tab-content\">\n                    <div class=\"tab-pane active\" id=\"allergy\">\n                        <h2>Allergy Types</h2>\n                        <hr>\n                        <div class=\"panel-group\" id=\"accordion\">\n                            ");
  foreach(AllergyType al_t, allergyTypes){;
  responsebody += tr("                                <div class=\"panel panel-default\">\n                                    <div class=\"panel-heading\">\n                                        <h4 class=\"panel-title\">\n                                            <a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#alT");
  responsebody += THttpUtility::htmlEscape(al_t.typeId());
  responsebody += tr("\">\n                                                ");
  responsebody += THttpUtility::htmlEscape(al_t.typeName());
  responsebody += tr("\n                                            </a>\n                                        </h4>\n                                    </div>\n                                    <div id=\"alT");
  responsebody += THttpUtility::htmlEscape(al_t.typeId());
  responsebody += tr("\" class=\"panel-collapse collapse\">\n                                        <div class=\"panel-body\">\n                                            ");
  responsebody += THttpUtility::htmlEscape(al_t.description());
  responsebody += tr("\n                                        </div>\n                                    </div>\n                                </div>\n                            ");
  };
  responsebody += tr("                        </div>\n                        <br>\n\n                        <h2>Allergy</h2>\n                        <hr>\n                        <div class=\"panel-group\" id=\"accordion2\">\n                            ");
  foreach(Allergy al, allergies){;
  responsebody += tr("                                <div class=\"panel panel-default\">\n                                    <div class=\"panel-heading\">\n                                        <h4 class=\"panel-title\">\n                                            <a data-toggle=\"collapse\" data-parent=\"#accordion2\" href=\"#al");
  responsebody += THttpUtility::htmlEscape(al.allergyId());
  responsebody += tr("\">\n                                                ");
  responsebody += THttpUtility::htmlEscape(al.allergyName());
  responsebody += tr("\n                                            </a>\n                                        </h4>\n                                    </div>\n                                    <div id=\"al");
  responsebody += THttpUtility::htmlEscape(al.allergyId());
  responsebody += tr("\" class=\"panel-collapse collapse\">\n                                        <div class=\"panel-body\">\n                                            ");
  responsebody += THttpUtility::htmlEscape(al.description());
  responsebody += tr("\n                                        </div>\n                                    </div>\n                                </div>\n                            ");
  };
  responsebody += tr("                        </div>\n\n                    </div>\n                    <div class=\"tab-pane\" id=\"drug\">\n                        <h2>Drugs</h2>\n                        <hr>\n                        <div class=\"panel-group\" id=\"accordion3\">\n                            ");
  foreach(Drug drug, drugs){;
  responsebody += tr("                                <div class=\"panel panel-default\">\n                                    <div class=\"panel-heading\">\n                                        <h4 class=\"panel-title\">\n                                            <a data-toggle=\"collapse\" data-parent=\"#accordion3\" href=\"#drug");
  responsebody += THttpUtility::htmlEscape(drug.drugId());
  responsebody += tr("\">\n                                                ");
  responsebody += THttpUtility::htmlEscape(drug.brandName());
  responsebody += tr("\n                                            </a>\n                                        </h4>\n                                    </div>\n                                    <div id=\"drug");
  responsebody += THttpUtility::htmlEscape(drug.drugId());
  responsebody += tr("\" class=\"panel-collapse collapse\">\n                                        <div class=\"panel-body\">\n                                            <h4><em><b>Ailments</b></em></h4>\n                                            ");
  responsebody += THttpUtility::htmlEscape(drug.ailment());
  responsebody += tr("\n                                            <br>\n\n                                            <h4><em><b>Ingredients</b></em></h4>\n                                            ");
  responsebody += THttpUtility::htmlEscape(drug.ingredients());
  responsebody += tr("\n                                            <br>\n\n                                            <h4><em><b>Dosage</b></em></h4>\n                                            ");
  responsebody += THttpUtility::htmlEscape(drug.dosage());
  responsebody += tr("\n                                            <br>\n\n                                            <h4><em><b>Side effects</b></em></h4>\n                                            ");
  responsebody += THttpUtility::htmlEscape(drug.sideEffects());
  responsebody += tr("\n                                        </div>\n                                    </div>\n                                </div>\n                            ");
  };
  responsebody += tr("                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>");

  return responsebody;
}

Q_DECLARE_METATYPE(news_indexView)
T_REGISTER_VIEW(news_indexView)

#include "news_indexView.moc"

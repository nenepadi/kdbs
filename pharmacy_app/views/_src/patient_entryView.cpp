#include <QtCore>
#include <TreeFrogView>
#include "patient.h" 
#include "allergy.h" 
#include "applicationhelper.h"

class T_VIEW_EXPORT patient_entryView : public TActionView
{
  Q_OBJECT
public:
  patient_entryView() : TActionView() { }
  patient_entryView(const patient_entryView &) : TActionView() { }
  QString toString();
};

QString patient_entryView::toString()
{
  responsebody.reserve(3979);
      tfetch(QVariantMap, patient);
  tfetch(QList<Allergy>, allergy_pull);
  responsebody += tr("\n<div class='panel panel-default'>\n  	<div class='panel-heading'>\n       	<i class='icon-plus icon-large'></i>\n       	New record\n   	</div>\n   	<div class='panel-body'>\n   		");
  responsebody += QVariant(formTag(urla("create"), Tf::Post, "", a("class", "form-horizontal"))).toString();
  responsebody += tr("\n   			<fieldset>\n   				<legend>Add new patient record ...</legend>\n                <div class='form-group'>\n                  	<label class='col-lg-2 control-label'>Full name</label>\n                  	<div class='col-lg-5'>\n                    	<input name='patient[patientLastname]' class='form-control' value='");
  responsebody += THttpUtility::htmlEscape(patient["patientLastname"]);
  responsebody += tr("' placeholder='Lastname' type='text' autofocus required>\n                  	</div>\n                  	<div class='col-lg-5'>\n                    	<input name='patient[patientOthernames]' class='form-control' value='");
  responsebody += THttpUtility::htmlEscape(patient["patientOthernames"]);
  responsebody += tr("' placeholder='Other names' type='text' required>\n                  	</div>\n                </div>\n                <div class='form-group'>\n                  	<label class='col-lg-2 control-label'>Date of birth</label>\n                  	<div class='col-lg-10'>\n                    	<input name='patient[patientDob]' class='form-control' \n                    	value='");
  responsebody += THttpUtility::htmlEscape(patient["patientDob"]);
  responsebody += tr("' placeholder='dd-mm-yyyy' type='date' required>\n                  	</div>\n                </div>\n                <div class='form-group'>\n                  	<label class='col-lg-2 control-label'>Gender</label>\n                  	<div class='radio'>\n                    	<div class='col-lg-2 col-lg-offset-1'>\n                    		<input name='patient[patientGender]' type='radio' value='Male' required> Male\n                    	</div>\n                    	<div class='col-lg-2'>\n                    		<input name='patient[patientGender]' type='radio' value='Female'> Female\n                    	</div>\n                  	</div>\n                </div>\n                <div class='form-group'>\n                  	<label class='col-lg-2 control-label'>Weight</label>\n                  	<div class='col-lg-10'>\n                    	<input name='patient[patientWeight]' class='form-control' placeholder='12.8' type='number' step='0.01'\n                        value='");
  responsebody += THttpUtility::htmlEscape(patient["patientWeight"]);
  responsebody += tr("' required>\n                  	</div>\n                </div>\n                <div class='form-group'>\n                  	<label class='col-lg-2 control-label'>Allergy</label>\n                  	<div class='col-lg-10'>\n                  		<select class='form-control' name='patient[allergy]' data-placeholder=\"Select an allergy\" id=\"allergy\">\n                            <option value=''></option>\n                  			<option value=''>None</option>\n                    		");
  foreach(Allergy al, allergy_pull){;
  responsebody += tr("                    		<option value='");
  responsebody += THttpUtility::htmlEscape(al.allergyId());
  responsebody += tr("'>");
  responsebody += THttpUtility::htmlEscape(al.allergyName());
  responsebody += tr("</option>\n                    		");
  };
  responsebody += tr("                  		</select>\n                  	</div>\n                </div>\n           	</fieldset>\n            <div class='form-actions'>\n            	<button class='btn btn-default' type='submit'>Save</button>\n                <a class='btn' href='/Patient/index'>Cancel</a>\n            </div>\n        </form>\n    </div>\n</div>\n\n<script type=\"text/javascript\">\n    $(function(){\n        $(\"#allergy\").chosen();\n    });\n</script>");

  return responsebody;
}

Q_DECLARE_METATYPE(patient_entryView)
T_REGISTER_VIEW(patient_entryView)

#include "patient_entryView.moc"

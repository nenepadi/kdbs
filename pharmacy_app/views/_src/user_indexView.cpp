#include <QtCore>
#include <TreeFrogView>
#include "user.h" 
#include "role.h" 
#include "applicationhelper.h"

class T_VIEW_EXPORT user_indexView : public TActionView
{
  Q_OBJECT
public:
  user_indexView() : TActionView() { }
  user_indexView(const user_indexView &) : TActionView() { }
  QString toString();
};

QString user_indexView::toString()
{
  responsebody.reserve(3041);
      tfetch(QList<User>, users);
  tfetch(QList<Role>, roles);
  responsebody += tr("<div class='panel panel-default grid'>\n	<div class='panel-heading'>\n		<i class='icon-table icon-large'></i>\n		Patient records\n		<div class='panel-tools'>\n			<div class='btn-group'>\n                <a class='btn' data-toggle='toolbar-tooltip' href='' title='Reload'>\n                    <i class='icon-refresh'></i>\n                </a>\n           	</div>\n        </div>\n   	</div>\n    <div class='panel-body filters'>\n    	<div class='row'>\n    		<div class='col-md-4'>\n                <a class='btn btn-primary' href='/User/entry'>\n                  	<i class='icon-plus'></i>\n                  	New user\n                </a>\n           	</div>\n           	<div class='col-md-3 col-md-offset-5'>\n               	<div class='input-group'>\n                  	<input class='form-control' placeholder='Quick search...' type='search'>\n                  	<span class='input-group-btn'>\n                    	<button class='btn' type='button'>\n                      		<i class='icon-search'></i>\n                    	</button>\n                  	</span>\n                </div>\n            </div>\n        </div>\n    </div>\n    <table class='table'>\n        <thead>\n            <tr>\n            	<th>#</th>\n            	<th>Full Name</th>\n    			<th>Username</th>\n    			<th>Role</th>\n                <th class='actions'>\n                  Actions\n                </th>\n            </tr>\n        </thead>\n        <tbody>\n        	");
  foreach(User user, users){;
  responsebody += tr("            <tr>\n            	<td>");
  responsebody += THttpUtility::htmlEscape(user.userId());
  responsebody += tr("</td>\n                <td>");
  responsebody += THttpUtility::htmlEscape(user.userLastname());
  responsebody += tr(", &nbsp;");
  responsebody += THttpUtility::htmlEscape(user.userOthernames());
  responsebody += tr("</td>\n                <td>");
  responsebody += THttpUtility::htmlEscape(user.username());
  responsebody += tr("</td>\n                <td>\n                ");
  foreach(Role role, roles){
                	if(user.role() == role.roleId()){
                		eh(role.roleName());
                	}
                };
  responsebody += tr("                </td>\n                <td class='action'>\n                  	<a class='btn btn-info' href='/User/edit/");
  responsebody += THttpUtility::htmlEscape(user.userId());
  responsebody += tr("' data-toggle='tooltip' title='Edit'>\n                    	<i class='icon-edit'></i>\n                  	</a>\n                  	");
  responsebody += QVariant(linkTo("<i class='icon-trash'></i>", urla("remove", user.userId()), Tf::Post, "confirm('Are you sure you want to delete this?')", a("class", "btn btn-danger") | a("data-toggle", "tooltip") | a("title", "Delete"))).toString();
  responsebody += tr("\n                </td>\n            </tr>\n            ");
  };
  responsebody += tr("        </tbody>\n    </table>\n    \n</div>\n");

  return responsebody;
}

Q_DECLARE_METATYPE(user_indexView)
T_REGISTER_VIEW(user_indexView)

#include "user_indexView.moc"

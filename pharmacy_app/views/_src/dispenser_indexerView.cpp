#include <QtCore>
#include <TreeFrogView>
#include "dispenser.h" 
#include "patient.h" 
#include "drug.h" 
#include "ailment.h" 
#include "applicationhelper.h"

class T_VIEW_EXPORT dispenser_indexerView : public TActionView
{
  Q_OBJECT
public:
  dispenser_indexerView() : TActionView() { }
  dispenser_indexerView(const dispenser_indexerView &) : TActionView() { }
  QString toString();
};

QString dispenser_indexerView::toString()
{
  responsebody.reserve(7313);
          tfetch(QVariantMap, disPen);
  tfetch(Patient, patient);
  tfetch(QList<Drug>, drugs);
  tfetch(QList<Ailment>, ailments);
  responsebody += tr("<div class='row'>\n    <div class='col-lg-12'>\n        <div class='panel panel-default'>\n            <div class='panel-heading'>\n                <i class='icon-shopping-cart icon-large'></i>\n                Dispensary process\n            </div>\n            <div class='panel-body'>\n                ");
  responsebody += QVariant(formTag(urla("create"), Tf::Post, "", a("class", "form-horizontal") | a("id", "disPenForms"))).toString();
  responsebody += tr("\n                    <fieldset>\n                        <legend>Dispensary process ...</legend>\n                        <div class='form-group'>\n                            <label for=\"patient\" class='col-lg-2 control-label'>Patient</label>\n                            <div class='col-lg-10'>\n                                <input name='disPen[patient]' class='form-control' placeholder='' type='text' value='");
  responsebody += THttpUtility::htmlEscape(patient.patientOthernames() + " " + patient.patientLastname());
  responsebody += tr("' readonly required>\n                            </div>\n                        </div>\n                        <div class='form-group'>\n                            <label class='col-lg-2 control-label'>Ailment</label>\n                            <div class='col-lg-10'>\n                                <select name='disPen[ailment]' class='form-control' required data-placeholder='Search through the symptoms a select one' id='ailment'>\n                                    <option value=\"\"></option>\n                                    ");
  foreach(Ailment ail, ailments){;
  responsebody += tr("                                    <option value='");
  responsebody += THttpUtility::htmlEscape(ail.name());
  responsebody += tr("'>\n                                        ");
  responsebody += THttpUtility::htmlEscape(ail.symptoms());
  responsebody += tr("\n                                    </option>\n                                    ");
  };
  responsebody += tr("                                </select>\n                            </div>\n                        </div>\n                        <div class='form-group'>\n                            <label class='col-lg-2 control-label'>Prescription</label>\n                            <div class='col-lg-10'>\n                                <select class='form-control drug' name='disPen[drugDispensed]' required data-placeholder='Select a drug...'>\n                                    <option value=''></option>\n                                    ");
  foreach(Drug drug, drugs){;
  responsebody += tr("                                    <option class='dgOpt' value='");
  responsebody += THttpUtility::htmlEscape(drug.drugId());
  responsebody += tr("' rel='");
  responsebody += THttpUtility::htmlEscape(drug.ailment());
  responsebody += tr("'>");
  responsebody += THttpUtility::htmlEscape(drug.brandName());
  responsebody += tr("</option>\n                                    ");
  };
  responsebody += tr("                                </select>\n                            </div>\n                        </div>\n                        <div class='form-group'>\n                            <div class='col-lg-10 col-lg-offset-2'>\n                                <select class='form-control dose' name='disPen[dosageGiven]' required data-placeholder='Select dosage for drug...'>\n                                    <option value=\"\"></option>\n                                    ");
  foreach(Drug dg, drugs){;
  responsebody += tr("                                    <optgroup label=\"");
  responsebody += THttpUtility::htmlEscape(dg.brandName());
  responsebody += tr("\" rel=\"");
  responsebody += THttpUtility::htmlEscape(dg.drugId());
  responsebody += tr("\">\n                                        ");
  if(dg.dosage() != NULL){;
  responsebody += tr("                                        <option value=\"");
  responsebody += THttpUtility::htmlEscape(dg.dosage());
  responsebody += tr("\">General - ");
  responsebody += THttpUtility::htmlEscape(dg.dosage());
  responsebody += tr("</option>\n                                        ");
  };
  responsebody += tr("\n                                        ");
  if(dg.dosageChildren() != NULL){;
  responsebody += tr("                                        <option value=\"");
  responsebody += THttpUtility::htmlEscape(dg.dosageChildren());
  responsebody += tr("\">Children - ");
  responsebody += THttpUtility::htmlEscape(dg.dosageChildren());
  responsebody += tr("</option>\n                                        ");
  };
  responsebody += tr("\n                                        ");
  if(dg.dosageAdult() != NULL){;
  responsebody += tr("                                        <option value=\"");
  responsebody += THttpUtility::htmlEscape(dg.dosageAdult());
  responsebody += tr("\">Adult - ");
  responsebody += THttpUtility::htmlEscape(dg.dosageAdult());
  responsebody += tr("</option>\n                                        ");
  };
  responsebody += tr("                                    </optgroup>\n                                    ");
  };
  responsebody += tr("                                </select>\n                            </div>\n                        </div>\n                    </fieldset>\n                    <div class='form-actions'>\n                        <button id='save' class='btn btn-default' type='submit'>Save</button>&nbsp;\n                        <a class='btn' href='/Dashboard'>Cancel</a>\n                    </div>\n                </form>\n            </div>\n        </div>\n    </div>\n</div>\n\n<script type=\"text/javascript\">\n    $(function(){\n        $('#ailment').chosen({\n            no_results_text: \"Please advice the patient to see a doctor!!! There is no drug for his/her description\",\n            search_contains: true\n        });\n\n        $('select.drug').chosen({\n            disable_search_threshold: 5\n        });\n\n        $('select.dose').chosen({\n            disable_search_threshold: 5\n        });\n\n        $('#ailment').change(function(){\n            $('.dgOpt').removeAttr('disabled');\n            ail = $(this).val();\n            $('select.drug .dgOpt').each(function(){\n                if($(this).attr('rel') != ail){\n                    $(this).attr('disabled', 'disabled');\n                }\n            });\n            $('select.drug').trigger('chosen:updated');\n        });\n\n        $('#ailment').on('chosen:no_results', function(evt, params){\n            var value = params.chosen.search_results.find('li.no-results').text();\n            var arr = value.split('\"');\n            alert(arr[0]);\n        });\n\n        $('select.drug').change(function(){\n            $('select.dose optgroup').removeAttr('disabled');\n            drug = $(this).val();\n            $('select.dose optgroup').each(function(){\n                if($(this).attr('rel') != drug){\n                    $(this).attr('disabled', 'disabled');\n                }\n            });\n            $('select.dose').trigger('chosen:updated');\n        });\n    });\n</script>");

  return responsebody;
}

Q_DECLARE_METATYPE(dispenser_indexerView)
T_REGISTER_VIEW(dispenser_indexerView)

#include "dispenser_indexerView.moc"

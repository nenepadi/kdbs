#include <QtCore>
#include <TreeFrogView>
#include "patient.h" 
#include "allergy.h" 
#include "applicationhelper.h"

class T_VIEW_EXPORT patient_editView : public TActionView
{
  Q_OBJECT
public:
  patient_editView() : TActionView() { }
  patient_editView(const patient_editView &) : TActionView() { }
  QString toString();
};

QString patient_editView::toString()
{
  responsebody.reserve(4703);
      tfetch(QVariantMap, patient);
  tfetch(QList<Allergy>, allergy_pull);
  responsebody += tr("\n<div class='panel panel-default'>\n    <div class='panel-heading'>\n        <i class='icon-edit icon-large'></i>\n        Patient #");
  responsebody += THttpUtility::htmlEscape(patient["patientId"]);
  responsebody += tr("'s record\n    </div>\n    <div class='panel-body'>\n      	");
  responsebody += QVariant(formTag(urla("save", patient["patientId"]), Tf::Post, "", a("class", "form-horizontal"))).toString();
  responsebody += tr("\n        	<fieldset>\n          		<legend>Editing ...</legend>\n            	<div class='form-group'>\n                	<label class='col-lg-2 control-label'>Full name</label>\n                	<div class='col-lg-5'>\n                  		<input name='patient[patientLastname]' class='form-control' \n                  		value='");
  responsebody += THttpUtility::htmlEscape(patient["patientLastname"]);
  responsebody += tr("' placeholder='Lastname' type='text' autofocus required>\n                	</div>\n                	<div class='col-lg-5'>\n                  		<input name='patient[patientOthernames]' class='form-control' \n                  		value='");
  responsebody += THttpUtility::htmlEscape(patient["patientOthernames"]);
  responsebody += tr("' placeholder='Other names' type='text' required>\n                	</div>\n            	</div>\n            	<div class='form-group'>\n                	<label class='col-lg-2 control-label'>Date of birth</label>\n                	<div class='col-lg-10'>\n                  		<input name='patient[patientDob]' class='form-control' \n                  		value='");
  responsebody += THttpUtility::htmlEscape(patient["patientDob"]);
  responsebody += tr("' placeholder='dd-mm-yyyy' type='date' required>\n                	</div>\n            	</div>\n            	<div class='form-group'>\n                	<label class='col-lg-2 control-label'>Gender</label>\n                	<div class='radio'>\n                  		<div class='col-lg-2 col-lg-offset-1'>\n                    		<input name='patient[patientGender]' type='radio' value='Male' required \n                    		");
  if(patient["patientGender"] == "Male"){;
  responsebody += tr("                    			checked\n                    		");
  };
  responsebody += tr("                    		> Male\n                  		</div>\n                  		<div class='col-lg-2'>\n                    		<input name='patient[patientGender]' type='radio' value='Female'\n                    		");
  if(patient["patientGender"] == "Female"){;
  responsebody += tr("                    			checked\n                    		");
  };
  responsebody += tr("                    		> Female\n                  		</div>\n                	</div>\n           		</div>\n            	<div class='form-group'>\n                	<label class='col-lg-2 control-label'>Weight</label>\n                	<div class='col-lg-10'>\n                  		<input name='patient[patientWeight]' class='form-control' placeholder='12.8' \n                  		value='");
  responsebody += THttpUtility::htmlEscape(patient["patientWeight"]);
  responsebody += tr("' type='number' step='0.01' required>\n                	</div>\n            	</div>\n            	<div class='form-group'>\n                	<label class='col-lg-2 control-label'>Allergy</label>\n                	<div class='col-lg-10'>\n                  		<select class='form-control' name='patient[allergy]' data-placement=\"Select an allergy\" id='allergy'>\n                    		<option value=''></option>\n                            <option value=''>None</option>\n                    		");
  foreach(Allergy al, allergy_pull){;
  responsebody += tr("                    			<option value='");
  responsebody += THttpUtility::htmlEscape(al.allergyId());
  responsebody += tr("'\n                    				");
  if(patient["allergy"] == al.allergyId()){;
  responsebody += tr("                    				selected\n                    				");
  };
  responsebody += tr(" >\n                    				");
  responsebody += THttpUtility::htmlEscape(al.allergyName());
  responsebody += tr("\n                    			</opstion>\n                    		");
  };
  responsebody += tr("                  		</select>\n                	</div>\n            	</div>\n        	</fieldset>\n            <div class='form-actions'>\n              	<button class='btn btn-default' type='submit'>Save</button>\n              	<a class='btn' href='/Patient/index'>Cancel</a>\n            </div>\n        </form>\n    </div>\n</div>\n\n<script type=\"text/javascript\">\n    $(function(){\n        $(\"#allergy\").chosen();\n    });\n</script>\n");

  return responsebody;
}

Q_DECLARE_METATYPE(patient_editView)
T_REGISTER_VIEW(patient_editView)

#include "patient_editView.moc"

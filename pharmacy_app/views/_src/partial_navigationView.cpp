#include <QtCore>
#include <TreeFrogView>
#include "applicationhelper.h"

class T_VIEW_EXPORT partial_navigationView : public TActionView
{
  Q_OBJECT
public:
  partial_navigationView() : TActionView() { }
  partial_navigationView(const partial_navigationView &) : TActionView() { }
  QString toString();
};

QString partial_navigationView::toString()
{
  responsebody.reserve(843);
  responsebody += tr("<!-- start sidebar -->\n<section id=\"sidebar\">\n	<i class=\"icon-align-justify icon-large\" id=\"toggle\"></i>\n	<ul id=\"dock\">\n		<li class=\"launcher\">\n			<i class=\"icon-dashboard\"></i>\n			<a href=\"/Dashboard\">Dashboard</a>\n		</li>\n		<li class=\"launcher\">\n			<i class=\"icon-group\"></i>\n			<a href=\"/User\">Users</a>\n		</li>\n		<li class=\"launcher\">\n			<i class=\"icon-ambulance\"></i>\n			<a href=\"/Patient\">Patients</a>\n		</li>\n		<li class=\"launcher\">\n			<i class=\"icon-medkit\"></i>\n			<a href=\"/Drug\">Drugs</a>\n		</li>\n		<li class=\"launcher\">\n			<i class=\"icon-info-sign\"></i>\n			<a href=\"/News\">Info</a>\n		</li>\n	</ul>\n	<div data-toggle=\"tooltip\" id=\"beaker\" title=\"\" data-original-title=\"DrugDispense &copy; 2015\"></div>\n</section>\n<!-- end sidebar -->");

  return responsebody;
}

Q_DECLARE_METATYPE(partial_navigationView)
T_REGISTER_VIEW(partial_navigationView)

#include "partial_navigationView.moc"

#include <QtCore>
#include <TreeFrogView>
#include "user.h" 
#include "applicationhelper.h"

class T_VIEW_EXPORT dashboard_indexView : public TActionView
{
  Q_OBJECT
public:
  dashboard_indexView() : TActionView() { }
  dashboard_indexView(const dashboard_indexView &) : TActionView() { }
  QString toString();
};

QString dashboard_indexView::toString()
{
  responsebody.reserve(1706);
    tfetch(User, logged_user);
  responsebody += tr("<div class='container'>\n    <div class='row well'>\n	    <div class='col-lg-2 center-block'>\n            <a class='btn' href='/Patient/entry'>\n              	<i class=\"icon-user icon-4x\"></i>\n              	<h5><b>Add patient</b></h5>\n            </a>\n       	</div>\n       	<div class='col-lg-2 center-block'>\n            <a class='btn' href='/Dispenser'>\n              	<i class='icon-medkit icon-4x icon-rotate-270'></i>\n              	<h5><b>Dispense drug</b></h5>\n            </a>\n       	</div>\n       	<div class='col-lg-2 center-block'>\n            <a class=\"btn ");
  if(logged_user.role() != 2){;
  responsebody += tr(" disabled ");
  };
  responsebody += tr("\" href='/Drug/entry'>\n              	<i class='icon-stethoscope icon-4x'></i>\n              	<h5><b>Add Drug</b></h5>\n            </a>\n       	</div>\n       	<div class='col-lg-2 center-block'>\n            <a class=\"btn ");
  if(logged_user.role() != 1){;
  responsebody += tr(" disabled ");
  };
  responsebody += tr("\" href='/User/entry'>\n              	<i class=\"icon-user-md icon-4x\"></i>\n              	<h5><b>Add user</b></h5>\n            </a>\n       	</div>\n       	<div class='col-lg-2 center-block'>\n            <a class='btn disabled' href=''>\n              	<i class='icon-user icon-4x'></i>\n              	<h5 class='disabled'><b>Add patient</b></h5>\n            </a>\n       	</div>\n       	<div class='col-lg-2 center-block'>\n            <a class='btn disabled' href=''>\n              	<i class='icon-user icon-4x'></i>\n              	<h5><b>Add patient</b></h5>\n            </a>\n       	</div>\n    </div>\n</div>\n    \n");

  return responsebody;
}

Q_DECLARE_METATYPE(dashboard_indexView)
T_REGISTER_VIEW(dashboard_indexView)

#include "dashboard_indexView.moc"

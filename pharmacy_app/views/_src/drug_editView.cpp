#include <QtCore>
#include <TreeFrogView>
#include "drug.h" 
#include "ailment.h" 
#include "applicationhelper.h"

class T_VIEW_EXPORT drug_editView : public TActionView
{
  Q_OBJECT
public:
  drug_editView() : TActionView() { }
  drug_editView(const drug_editView &) : TActionView() { }
  QString toString();
};

QString drug_editView::toString()
{
  responsebody.reserve(6155);
      tfetch(QVariantMap, drug);
  tfetch(QList<Ailment>, ailments);
  responsebody += tr("<div class='panel panel-default'>\n    <div class='panel-heading'>\n        <i class='icon-edit icon-large'></i>\n        Edit record\n    </div>\n    <div class='panel-body'>\n        ");
  responsebody += QVariant(formTag(urla("create"), Tf::Post, "", a("class", "form-horizontal"))).toString();
  responsebody += tr("\n            <fieldset>\n                <legend>Edit record <b>");
  responsebody += THttpUtility::htmlEscape(drug["drugId"]);
  responsebody += tr("</b></legend>\n                <div class='form-group'>\n                    <label class='col-lg-2 control-label'>Brand name</label>\n                    <div class='col-lg-10'>\n                        <input name='drug[brandName]' class='form-control' value='");
  responsebody += THttpUtility::htmlEscape(drug["brandName"]);
  responsebody += tr("' placeholder='Brand name' type='text' autofocus required>\n                    </div>\n                </div>\n                <div class='form-group'>\n                    <label class='col-lg-2 control-label'>Manufacturer</label>\n                    <div class='col-lg-10'>\n                        <input name='drug[manufacturer]' class='form-control' value='");
  responsebody += THttpUtility::htmlEscape(drug["manufacturer"]);
  responsebody += tr("' placeholder='Manufacturing company' type='text' required>\n                    </div>\n                </div>\n                <div class='form-group'>\n                    <label class='col-lg-2 control-label'>Expiry date</label>\n                    <div class='col-lg-10'>\n                        <input name='drug[expiryDate]' class='form-control' value='");
  responsebody += THttpUtility::htmlEscape(drug["expiryDate"]);
  responsebody += tr("' placeholder='dd-mm-yyyy' type='date' required>\n                    </div>\n                </div>\n                <div class='form-group'>\n                    <label class='col-lg-2 control-label'>Active ingredients</label>\n                    <div class='col-lg-10'>\n                        <textarea name='drug[ingredients]' value='");
  responsebody += THttpUtility::htmlEscape(drug["ingredients"]);
  responsebody += tr("' rows='2' class='form-control tiny' required></textarea>\n                        <p class=\"help-block\">enter all the active ingredients here. Please seperate them by comma.</p>\n                    </div>\n                </div>\n                <div class='form-group'>\n                    <label class='col-lg-2 control-label'>Ailments</label>\n                    <div class='col-lg-10'>\n                        <select name='drug[ailment]' value='");
  responsebody += THttpUtility::htmlEscape(drug["ailment"]);
  responsebody += tr("' class='form-control' required data-placeholder=\"Select an ailment\" id='ailment'>\n                            <option value=''></option>\n                            ");
  foreach(Ailment ail, ailments){;
  responsebody += tr("                                <option value='");
  responsebody += THttpUtility::htmlEscape(ail.name());
  responsebody += tr("' \n                                ");
  if(drug["ailment"] == ail.name()){;
  responsebody += tr("                                    selected\n                                ");
  };
  responsebody += tr(">");
  responsebody += THttpUtility::htmlEscape(ail.name());
  responsebody += tr("</option>\n                            ");
  };
  responsebody += tr("                        </select>\n                    </div>\n                </div>\n                <div class='form-group'>\n                    <label class='col-lg-2 control-label'>\n                        General dosage\n                    </label>\n                    <div class='col-lg-10'>\n                        <input type='text' name='drug[dosage]' value='");
  responsebody += THttpUtility::htmlEscape(drug["dosage"]);
  responsebody += tr("' class='form-control' />\n                        <p class=\"help-block\">Put in a general dosage here</p>\n                    </div>\n                </div>\n                <div class='form-group'>\n                    <label class='col-lg-2 control-label'>\n                        Dosage for children\n                    </label>\n                    <div class='col-lg-10'>\n                        <input type='text' name='drug[dosageChildren]' value='");
  responsebody += THttpUtility::htmlEscape(drug["dosageChildren"]);
  responsebody += tr("' class='form-control' />\n                        <p class=\"help-block\">Put in a dosage that is specific to children</p>\n                    </div>\n                </div>\n                <div class='form-group'>\n                    <label class='col-lg-2 control-label'>\n                        Dosage for adults\n                    </label>\n                    <div class='col-lg-10'>\n                        <input type='text' name='drug[dosageAdult]' value='");
  responsebody += THttpUtility::htmlEscape(drug["dosageAdult"]);
  responsebody += tr("' class='form-control' />\n                        <p class=\"help-block\">Put in a dosage specific for adults</p>\n                    </div>\n                </div>\n                <div class='form-group'>\n                    <label class='col-lg-2 control-label'>Side effects</label>\n                    <div class='col-lg-10'>\n                        <input name='drug[sideEffects]' value='");
  responsebody += THttpUtility::htmlEscape(drug["sideEffects"]);
  responsebody += tr("' class='form-control' required />\n                        <p class=\"help-block\">What are the side effects to the drug. Put in the major one</p>\n                    </div>\n                </div>\n            </fieldset>\n            <div class='form-actions'>\n                <button class='btn btn-default' type='submit'>Save</button>\n                <a class='btn' href='/Drug'>Cancel</a>\n            </div>\n        </form>\n    </div>\n</div>\n\n<script type=\"text/javascript\">\n    $(function(){\n        $(\"#ailment\").chosen();\n    });\n</script>");

  return responsebody;
}

Q_DECLARE_METATYPE(drug_editView)
T_REGISTER_VIEW(drug_editView)

#include "drug_editView.moc"

#include <QtCore>
#include <TreeFrogView>
#include "drug.h" 
#include "patient.h" 
#include "dispenser.h" 
#include "user.h" 
#include "role.h" 
#include "allergy.h" 
#include "applicationhelper.h"

class T_VIEW_EXPORT dispenser_viewReceiptView : public TActionView
{
  Q_OBJECT
public:
  dispenser_viewReceiptView() : TActionView() { }
  dispenser_viewReceiptView(const dispenser_viewReceiptView &) : TActionView() { }
  QString toString();
};

QString dispenser_viewReceiptView::toString()
{
  responsebody.reserve(3456);
              tfetch(QList<Allergy>, allergies);
  tfetch(QList<Role>, roles);
  tfetch(User, logged_user);
  tfetch(QList<Drug>, drugs);
  tfetch(Patient, pat);
  tfetch(QList<Dispenser>, pat_recentDispense);
  tfetch(int, count);
  responsebody += tr("<div class='panel panel-default'>\n    <div class='panel-heading'>\n        <i class='icon-info-sign icon-large'></i>\n        Prescription summary\n        <div class='panel-tools'>\n            <div class='btn-group'>\n                <a class='btn' data-toggle='toolbar-tooltip' href='/Dashboard' title='Back'>\n                    <i class='icon-double-angle-left'></i> Back\n                </a>\n            </div>\n        </div>\n    </div>\n    <div class='panel-body'>\n        <div class=\"row\">\n            <div class=\"col-xs-6\">\n                <address>\n                <strong>Prescribed &amp; Issued by:</strong><br>\n                    ");
  responsebody += THttpUtility::htmlEscape(logged_user.userOthernames());
  responsebody += tr("&nbsp;");
  responsebody += THttpUtility::htmlEscape(logged_user.userLastname());
  responsebody += tr("<br>\n                    ");
  foreach(Role role, roles){
                        if(logged_user.role() == role.roleId()){
                            eh(role.roleName());
                        }
                    };
  responsebody += tr("<br>\n                    ");
  responsebody += THttpUtility::htmlEscape(QDate::currentDate());
  responsebody += tr("\n                </address>\n            </div>\n            <div class=\"col-xs-6 text-right\">\n                <address>\n                <strong>Prescribed To:</strong><br>\n                    ");
  responsebody += THttpUtility::htmlEscape(pat.patientOthernames());
  responsebody += tr("&nbsp;");
  responsebody += THttpUtility::htmlEscape(pat.patientLastname());
  responsebody += tr("<br>\n                    ");
  foreach(Allergy all, allergies){
                        if(pat.allergy() == all.allergyId()){
                            eh(all.allergyName());
                        }
                    };
  responsebody += tr("                </address>\n            </div>\n        </div>\n        <table class='table table-condensed'>\n            <thead>\n                <tr>\n                    <th>#</th>\n                    <th>Ailment</th>\n                    <th>Drug</th>\n                    <th>Dosage</th>\n                </tr>\n            </thead>\n            <tbody>\n                ");
  foreach(Dispenser dispenser, pat_recentDispense){;
  responsebody += tr("                    <td>");
  responsebody += THttpUtility::htmlEscape(count++);
  responsebody += tr("</td>\n                    <td>");
  responsebody += THttpUtility::htmlEscape(dispenser.ailment());
  responsebody += tr("</td>\n                    ");
  foreach(Drug drug, drugs){;
  responsebody += tr("                        ");
  if(drug.drugId() == dispenser.drugDispensed()){;
  responsebody += tr("                            <td>");
  responsebody += THttpUtility::htmlEscape(drug.brandName());
  responsebody += tr("</td>\n                        ");
  };
  responsebody += tr("                    ");
  };
  responsebody += tr("                    <td>");
  responsebody += THttpUtility::htmlEscape(dispenser.dosageGiven());
  responsebody += tr("</td>\n                ");
  };
  responsebody += tr("            </tbody>\n        </table>\n    </div>\n</div>");

  return responsebody;
}

Q_DECLARE_METATYPE(dispenser_viewReceiptView)
T_REGISTER_VIEW(dispenser_viewReceiptView)

#include "dispenser_viewReceiptView.moc"

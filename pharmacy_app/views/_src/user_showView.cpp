#include <QtCore>
#include <TreeFrogView>
#include "applicationhelper.h"

class T_VIEW_EXPORT user_showView : public TActionView
{
  Q_OBJECT
public:
  user_showView() : TActionView() { }
  user_showView(const user_showView &) : TActionView() { }
  QString toString();
};

QString user_showView::toString()
{
  responsebody.reserve(0);

  return responsebody;
}

Q_DECLARE_METATYPE(user_showView)
T_REGISTER_VIEW(user_showView)

#include "user_showView.moc"
